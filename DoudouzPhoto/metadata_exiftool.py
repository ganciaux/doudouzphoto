#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Module containing class MetaDataManagerExiftool
"""

import subprocess
# -------------------------------------------------------------------------- #


class MetaDataManagerExiftool:

    """
class MetaDataManagerExifTool: Documentation TODO
    """

    def __init__(self, filename):
        """ Constructor """

        self.filename = filename
        self.key_list = []
        self.tags = dict()
        p = subprocess.Popen(
            ["exiftool", "-s", "-E", self.filename],
            stdout=subprocess.PIPE)
        out, err = p.communicate()
        for l in out.decode('utf8').split('\n'):
            k = l.split(':')[0]
            k = k.strip()
            self.key_list.append(k)

    def setTag(self, key, value):
        """ set a tag """
        self.tags[key] = value
        cmd = 'exiftool -{0}=\'{1}\' \'{2}\''.format(
            key, value, self.filename)
        # print('AAAA', cmd)
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
        out, err = p.communicate()

    def getTag(self, key):
        """ get a tag """
        if key not in self.key_list:
            raise KeyError("no such tag: {0}".format(key))
        if key in self.tags:
            return self.tags[key]

        p = subprocess.Popen(["exiftool", "-{0}".format(key),
                              "-b", self.filename],
                             stdout=subprocess.PIPE)
        out, err = p.communicate()
        v = out.decode('utf8')
        if v == "":
            raise KeyError("no such tag: {0}".format(key))
        # print('AAAAAAAAAAAAAAa', v)
        self.tags[key] = v
        return v

    def __iter__(self):
        return self.tags.__iter__()
