#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Grapical module for scanning a directory
"""

from photo_db import PhotoDB
from photo_db_select import PhotoDBSelect
from photo_db_gui_action import PhotoDBActionDialog
from structured_importer import structured_import
from PyQt4 import QtGui, QtCore
import sys
import os

################################################################


class PhotoDBScan(QtGui.QWidget):

    def __init__(self, mother):
        QtGui.QWidget.__init__(self, mother)

        self.mother = mother
        self.button_scan = QtGui.QPushButton('Scan', self)
        self.button_scan_reset = QtGui.QPushButton('Reset and Scan', self)
        self.button_structured_import = QtGui.QPushButton(
            'Structured Import', self)
        self.layout = QtGui.QVBoxLayout(self)

        self.setLayout(self.layout)
        self.layout.addWidget(self.button_scan)
        self.layout.addWidget(self.button_scan_reset)
        self.layout.addWidget(self.button_structured_import)
        self.button_scan.clicked.connect(self.scan)
        self.button_scan_reset.clicked.connect(self.scan_reset)
        self.button_structured_import.clicked.connect(self.structured_import)
        self.show()

    def setDBPath(self, path):
        self.db_path = str(path.toUtf8())

    def action(self, printInfo, setPercent, exceptionSignal):

        db = PhotoDB(self.db_path)
        db.printInfo = printInfo
        db.scanPhotos()
        printInfo('Done Scanning', True)

    def scan_reset(self):
        os.remove(os.path.join(self.db_path, "DoudouzPhoto.db"))
        self.scan()

    def structured_import_action(self, printInfo, setPercent, exceptionSignal):
        structured_import(self.db_path, self.events)
        del self.events

    def structured_import(self):
        os.remove(os.path.join(self.db_path, "DoudouzPhoto.db"))
        self.events, valid = QtGui.QInputDialog.getText(
            self,
            'Event selection',
            'Please select the main event of this directory')
        print(self.events)
        self.events = str(self.events)
        self.events = self.events.split(' ')

        progress = PhotoDBActionDialog(progress=False)
        progress.finished.connect(self.mother.base_selector.set_db_path)
        progress.exec_("Structured Import in progress",
                       self.structured_import_action)

    def scan(self):
        progress = PhotoDBActionDialog(progress=False)
        progress.finished.connect(self.mother.base_selector.set_db_path)
        progress.exec_("Scanning in progress", self.action)


################################################################

def main():

    app = QtGui.QApplication(sys.argv)
    scanner = PhotoDBScan()
    base_selector = PhotoDBSelect()
    base_selector.changedDB.connect(scanner.setDBPath)
    base_selector.set_db_path()
    app.exec_()


if __name__ == '__main__':
    main()
