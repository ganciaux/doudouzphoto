#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Grapical module for a list of Photos
"""

from photo import Photo
from photo_db import PhotoDB
from photo_organizer import PhotoOrganizer
from photo_db_sync import PhotoDBSynch
from photo_db_select import PhotoDBSelect
from photo_db_gui_list_photo import PhotoDBListPhoto
from photo_db_gui_action import PhotoDBActionDialog
from PyQt4 import QtGui, QtCore
import sys
import os
import pickle
from datetime import date, timedelta

################################################################


class PhotoDBPhotoSelector(QtGui.QWidget):
    def __init__(self, mother):
        self.mother = mother
        QtGui.QWidget.__init__(self, mother)
        self.date_start = QtGui.QDateEdit(QtCore.QDate(1900, 1, 1), self)
        self.date_start.setCalendarPopup(True)
        self.date_start_label = QtGui.QLabel(self)
        self.date_start_label.setText("Date Start Selection")

        self.date_end = QtGui.QDateEdit(date.today() + timedelta(days=1), self)
        self.date_end.setCalendarPopup(True)
        self.date_end_label = QtGui.QLabel(self)
        self.date_end_label.setText("Date End Selection")
        self.event = QtGui.QComboBox(self)
        self.event_label = QtGui.QLabel(self)
        self.event_label.setText("Event Selection")
        self.button_select = QtGui.QPushButton('Select photos', self)
        self.list_photos = PhotoDBListPhoto(self)
        self.button_extract = QtGui.QPushButton('Extract Photos', self)
        self.button_trash = QtGui.QPushButton('Send to trash', self)

        self.layout = QtGui.QVBoxLayout(self)
        self.setLayout(self.layout)
        self.layout.addWidget(self.date_start_label)
        self.layout.addWidget(self.date_start)
        self.layout.addWidget(self.date_end_label)
        self.layout.addWidget(self.date_end)
        self.layout.addWidget(self.event_label)
        self.layout.addWidget(self.event)
        self.layout.addWidget(self.button_select)
        self.layout.addWidget(self.list_photos)
        self.layout.addWidget(self.button_extract)
        self.layout.addWidget(self.button_trash)
        self.show()

        self.button_select.clicked.connect(self.fetchPhotoList)
        self.button_extract.clicked.connect(self.extract)
        self.button_trash.clicked.connect(self.sendToTrash)

    def setDBPath(self, path):
        self.db_path = str(path.toUtf8())
        self.updateCombo()

    def updateCombo(self):
        db = PhotoDB(self.db_path)
        events = db.getAllEvents()
        events = [self.trUtf8(e) for e in events]
        self.event.clear()
        self.event.insertItems(0, events)
        index = self.event.findText('')
        self.event.setCurrentIndex(index)

    def fetchHashListAction(self, printInfo, setPercent, exceptionSignal):

        db = PhotoDB(self.db_path)
        start_date = self.getStartDate()
        end_date = self.getEndDate()
        event = self.getEvent()

        self.photo_hash_list = [h for h in db.iterateHash(
            start_date=start_date, end_date=end_date, event=event)]

    def fetchPhotoList(self):

        progress = PhotoDBActionDialog(progress=False, auto_close=True)
        progress.exec_("Fectching Hash List", self.fetchHashListAction)
        self.list_photos.setList(self.photo_hash_list, self.db_path)

    def getStartDate(self):
        d = self.date_start.dateTime().toPyDateTime()
        return d

    def getEndDate(self):
        d = self.date_end.dateTime().toPyDateTime()
        return d

    def getEvent(self):
        e = unicode(self.event.currentText()).encode('utf8')
        if e == '':
            return None
        return e

    def extractAction(self, printInfo, setPercent, exceptionSignal):
        db_src = PhotoDB(db_path=self.db_path)
        db_src.printInfo = printInfo
        db_dst = PhotoDB(db_path=self.db_path_dst)
        db_dst.printInfo = printInfo
        synch = PhotoDBSynch(db_src, db_dst)
        synch.printInfo = printInfo
        synch.synchFromHashList(self.photo_hash_list)
        print("AAAAAA" + str(type(self.db_path_dst)))
        self.mother.base_selector.conf[self.mother.base_selector.conf_key] = \
            self.db_path_dst
        print(self.mother.base_selector.getDBPath())

    def extract(self):
        self.db_path_dst = str(
            QtGui.QFileDialog.getExistingDirectory(self, 'Open Directory'))
        progress = PhotoDBActionDialog(progress=False, auto_close=True)
        progress.finished.connect(self.mother.base_selector.set_db_path)
        progress.exec_("Extract Photos", self.extractAction)

    def sendToTrash(self):
        db = PhotoDB(db_path=self.db_path)
        photo_hash = self.list_photos.getSelectedPhotoHash()
        print(self.db_path)
        print(photo_hash)
        db.moveToTrashFromHash(photo_hash)


################################################################

def main():

    app = QtGui.QApplication(sys.argv)
    photo_selector = PhotoDBPhotoSelector()

    base_selector = PhotoDBSelect()
    base_selector.changedDB.connect(photo_selector.setDBPath)
    base_selector.set_db_path()

    app.exec_()


if __name__ == '__main__':
    main()
