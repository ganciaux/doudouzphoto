#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Executable to quickly import photos to a database
"""


# parse parameters
import sys
import guess_date
import photo_db
import photo_organizer
import photo_db_sync
import secureDeletePhoto

directory_to_import = sys.argv[1]
db_where_to_push = sys.argv[2]
events = None
if len(sys.argv) > 3:
    events = sys.argv[3:]
else:
    events = None

# checks for stupididy
if directory_to_import == db_where_to_push:
    raise Exception("imported and importer are same: abort")

# first build the local database and fill it
db_src = photo_db.PhotoDB(db_path=directory_to_import)
db_src.scanPhotos(recompute_hash=False)

# check that all photos have a correct date
for photo in db_src:
    fname = photo.getFilename()
    date = photo.getDate()
    if date is None:
        date = guess_date.guessDate(fname)
    if date is None:
        raise Exception('{0}: has no date -> abort import'.format(fname))

# set the event
if events is not None:
    for photo in db_src:
        photo.setEvents(events)

# organize the local base
organizer = photo_organizer.PhotoOrganizer(db_src)
organizer.reorganize()
organizer.cleanEmptyDirectories()

# synch for the database
db_dst = photo_db.PhotoDB(db_path=db_where_to_push)
synch = photo_db_sync.PhotoDBSynch(db_src, db_dst)
synch.synch()

# clean the local files
eraser = secureDeletePhoto.PhotoSecureDelete(db_src, db_dst)
eraser.cleanDuplicates(True)
