#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Read the defaults from conf files
"""

import sys
import os
import pickle

################################################################


class PhotoDBConf:

    def __init__(self):
        self.conf_file = os.path.expanduser('~/.DoudouzPhoto.config')
        self.conf = dict()
        if os.path.exists(self.conf_file):
            self.conf = pickle.load(open(self.conf_file))

    def __del__(self):
        print "AAAAAAAAAAAa "
        with open(self.conf_file, 'w') as f:
            self.conf = pickle.dump(self.conf, f)
