#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------- #
"""
Module containing class photo list
"""
# ----------------------------------------------------------- #
import os
import re
import fileinput
# ----------------------------------------------------------- #


class PhotoList(object):
    """ This class is meant to store a list of media files """

    _valid_extensions = ['jpeg', 'jpg', 'nef', 'cr2',
                         'mov', '3gp', 'avi', 'mp4']

    def __init__(self, photo_list=None):
        if photo_list is None:
            photo_list = []
        self._photo_list = photo_list

    def add_photo(self, filename):
        " adding a photo to the list "

        self._photo_list.append(filename)

    def read_from_standard_input(self):
        for filename in fileinput.input('-'):
            filename = filename.strip()
            dummy_base, ext = os.path.splitext(filename)
            ext = ext.lower()[1:]
            if ext not in self._valid_extensions:
                continue
            self.add_photo(filename)

    def recursive_fill(self, _dir):
        " adding photos recursively from a directory "

        for root, dummy_dirs, files in os.walk(_dir):

            if re.match(r'.*\.thumbnails.*', root):
                continue

            for _file in files:

                dummy_base, ext = os.path.splitext(_file)
                ext = ext.lower()[1:]
                if ext not in self._valid_extensions:
                    continue
                full_name = os.path.join(root, _file)
                if os.path.getsize(full_name) == 0:
                    continue
                self.add_photo(full_name)

    def make_files_relative(self, root):
        self._photo_list = [
            os.path.relpath(f, root) for f in self._photo_list]

    def __str__(self):
        return "\n".join(self._photo_list)

    def __iter__(self):
        return self._photo_list.__iter__()

    def __len__(self):
        return len(self._photo_list)

# -------------------------------------------------------------------------- #


def main():
    " main function: make a test "

    photo_list = PhotoList()
    photo_list.read_from_standard_input()
    print(photo_list)

# -------------------------------------------------------------------------- #


if __name__ == '__main__':
    main()
