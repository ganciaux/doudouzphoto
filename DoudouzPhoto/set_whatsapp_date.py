#!/usr/bin/env python3
################################################################
import os
import re
from datetime import datetime
from DoudouzPhoto.photo import Photo
from DoudouzPhoto.photo_list import PhotoList
from DoudouzPhoto.photo_db import PhotoDB
from DoudouzPhoto.photo_proxy import PhotoNotInDataBase
################################################################


def main():
    photo_db = PhotoDB(db_path=os.getcwd())
    photo_list = PhotoList()
    photo_list.read_from_standard_input()

    print('done')

    for fname in photo_list:
        if not os.path.isfile(fname):
            continue
        basename = os.path.basename(fname)
        m = re.match('(\./)?(IMG|VID)-([0-9]+)-WA([0-9]+)\..*', basename)
        if not m:
            continue
        # print(m.groups())
        date = m.group(3)
        year = date[:4]
        month = date[4:6]
        day = date[6:]
        date = '{0}-{1}-{2}'.format(year, month, day)
        date = datetime.strptime(date, "%Y-%m-%d")
        photo = Photo.createPhoto(fname)

        if photo.getDate() is None:
            print('set:', fname, date)
            photo.setDate(date)
        date = photo.getDate()
        photo_hash = photo.getHash()
        try:
            photo_proxy = photo_db.getPhotoProxyFromHash(photo_hash)
        except PhotoNotInDataBase:
            print(
                f"Could not find {fname} in database: need to scan database ?")
            continue
        photo_proxy.setDate(date)


if __name__ == '__main__':
    main()
