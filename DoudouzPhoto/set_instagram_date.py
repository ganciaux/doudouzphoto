#!/usr/bin/env python3
################################################################
import os
import re
from datetime import datetime
from DoudouzPhoto.photo import Photo
from DoudouzPhoto.photo_list import PhotoList
from DoudouzPhoto.photo_db import PhotoDB
################################################################

photo_db = PhotoDB(db_path=os.getcwd())
photo_list = PhotoList()
photo_list.read_from_standard_input()

print('done')

for fname in photo_list:
    if not os.path.isfile(fname):
        continue
    basename = os.path.basename(fname)
    m = re.match('(\./)?(IMG|VID)_([0-9]+)_([0-9]+)\..*', basename)
    if not m:
        continue
    # print(fname)
    # print(m.groups())
    date = m.group(3)
    year = date[:4]
    month = date[4:6]
    day = date[6:]
    date = '{0}-{1}-{2}'.format(year, month, day)
    try:
        date = datetime.strptime(date, "%Y-%m-%d")
    except Exception as e:
        print('pb with {0}'.format(fname))
        print(e)
        continue
    photo = Photo.createPhoto(fname)

    if photo.getDate() is None:
        print('set:', fname, date)
        photo.setDate(date)
    date = photo.getDate()
    photo_hash = photo.getHash()
    photo_proxy = photo_db.getPhotoProxyFromHash(photo_hash)
    photo_proxy.setDate(date)
