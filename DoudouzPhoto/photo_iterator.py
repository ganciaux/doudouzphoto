#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Module for iterating over photos
"""

from .photo import Photo
# -------------------------------------------------------------------------- #


class PhotoIterator:

    def __init__(self, photo_db, it_hash=False, it_trash=False,
                 start_date=None, end_date=None, event=None, **kwargs):
        """ Constructor of the iterator """

        self.it_hash = it_hash
        self.it_trash = it_trash
        self.photo_db = photo_db

        if it_trash is False:
            self.__init__PhotoIterator(photo_db, it_hash,
                                       start_date, end_date,
                                       event, **kwargs)

        else:
            self.__init__PhotoTrash(photo_db, **kwargs)

    def __init__PhotoTrash(self, photo_db, **kwargs):

        request = "SELECT photoid FROM trashBin"
        photo_db.execute(request)
        self._hash_list = [str(h[0])
                           for h in self.photo_db._cursor_db.fetchall()]

        self._internal_iterator = self._hash_list.__iter__()

    def __init__PhotoIterator(self, photo_db, it_hash,
                              start_date, end_date, event, **kwargs):

        self.photo_db = photo_db
        request = ""
        if start_date:
            request += "creation_date >= {0}".format(
                Photo.getTimeStamp(start_date))
        if end_date:
            if not request == "":
                request += " and "

            request += "creation_date <= {0}".format(
                Photo.getTimeStamp(end_date))
        if not request == '':
            # request = '((' + request + ") or creation_date = 'None')"
            request = '((' + request + "))"
        if event:
            if not request == "":
                request += " and "
            request += """
eventid in (SELECT eventid from events WHERE eventname == '{0}')
""".format(event)

        if not request == "":
            request = "SELECT hash FROM photos WHERE " + request
        else:
            request = "SELECT hash FROM photos"
        request += " ORDER BY creation_date DESC"
        photo_db.execute(request)
        self._hash_list = [str(h[0])
                           for h in self.photo_db._cursor_db.fetchall()]

        self._internal_iterator = self._hash_list.__iter__()
        self.it_hash = it_hash

    def __iter__(self):
        return self

    def __next__(self):
        photo_hash = self._internal_iterator.__next__()

        if self.it_hash or self.it_trash:
            return photo_hash

        photo = self.photo_db.getPhotoProxyFromHash(photo_hash)
        return photo

    def next(self):
        return self.__next__()
