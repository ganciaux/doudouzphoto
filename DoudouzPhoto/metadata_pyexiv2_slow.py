#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Module containing class MetaDataManagerInterface
"""

from pyexiv2 import *
import subprocess
import sys
import os

# -------------------------------------------------------------------------- #


class MetaDataManagerPyExiv2:

    """
class MetaDataManager: Documentation TODO
    """

    def __init__(self, filename):
        """ Constructor """
        self.filename = filename
        self.key_list = dict()
        self.script = os.path.join(os.path.dirname(
            sys.argv[0]), 'metadata_pyexiv2.py')

        # self.exiv2_metadata = ImageMetadata(filename)
        # self.exiv2_metadata.read()
        # self.key_list = [k for k in self.exiv2_metadata]

    def setTag(self, key, value):
        """ set a tag """
        keys = key.split('.')
        if not keys[0] == 'Exif':
            key = 'Exif.Photo.' + key
        p = subprocess.Popen(
            "python metadata_pyexiv2.py --tag {0} --value=\"{1}\" {2}".format(
                key, value, self.filename), stdout=subprocess.PIPE, shell=True)
        out, err = p.communicate()

    def getTag(self, key):
        """ set a tag """
        keys = key.split('.')
        # print key

        if not keys[0] == 'Xmp' and not keys[0] == 'Exif':
            key = 'Exif.Photo.' + key
        # print key
        p = subprocess.Popen("python {2} --tag {0} {1}".format(
            key, self.filename, self.script),
            stdout=subprocess.PIPE, shell=True)
        out, err = p.communicate()
        v = out.strip()
        self.key_list[key] = v
        return v

        return out

    def __iter__(self): return self.key_list.__iter__()


def main():

    import argparse

    parser = argparse.ArgumentParser(
        description='small pyexiv2 standalone parser')
    parser.add_argument('filename', type=str,
                        help='The path of the file to treat')
    parser.add_argument('--tag', type=str, help='The tag to get')
    parser.add_argument('--value', type=str, help='set the value of the tag')
    args = vars(parser.parse_args())
    filename = args['filename']
    exiv2_metadata = ImageMetadata(filename)
    exiv2_metadata.read()

    if args['tag'] is None:
        return
    # print args
    key = args['tag']
    if args['value'] is not None:
        value = args['value']
        exiv2_metadata[key] = ExifTag(key, value)
        exiv2_metadata.write()
    else:
        try:
            v = exiv2_metadata[key].value
        except:
            v = exiv2_metadata[key].raw_value
        print(v)


if __name__ == '__main__':
    main()
