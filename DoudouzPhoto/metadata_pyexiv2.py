#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Module containing class MetaDataManagerInterface
"""

import sys
from pyexiv2 import ImageMetadata
from pyexiv2 import ExifTag
from pyexiv2 import ExifValueError
import argparse
# ---------------------------------------------------------------------- #


class MetaDataManagerPyExiv2:

    """
class MetaDataManager: Documentation TODO
    """

    def __init__(self, filename):
        """ Constructor """
        self.filename = filename
        self.exiv2_metadata = ImageMetadata(filename)
        self.exiv2_metadata.read()
        self.key_list = [k for k in self.exiv2_metadata]

    def setTag(self, key, value):
        """ set a tag """
        keys = key.split('.')
        if not keys[0] == 'Exif':
            key = 'Exif.Photo.' + key
        self.exiv2_metadata[key] = ExifTag(key, value)
        self.exiv2_metadata.write()

    # def setTag(self, key, value):
    #     """ set a tag """
    #     keys = key.split('.')
    #     if not keys[0] == 'Exif':
    #         key = 'Exif.Photo.' + key
    #     args = ['--tag', key, '--value="{0}"'.format(value), '{0}'.format(
    # self.filename)]
    #     main(args)
    #     #p = subprocess.Popen(
    # 'python {3} --tag {0} --value="{1}" "{2}"'.format(
    #     #    key, value, self.filename, self.script),
    #     #                     stdout=subprocess.PIPE, shell=True)
    #     #out, err = p.communicate()

    def getTag(self, key):
        """ set a tag """
        keys = key.split('.')

        if not keys[0] == 'Xmp' and not keys[0] == 'Exif':
            key = 'Exif.Photo.' + key

        entry = self.exiv2_metadata[key]
        try:
            v = entry.value
        except ExifValueError:
            v = entry.raw_value
        except NotImplementedError:
            v = entry.raw_value

        if isinstance(v, bytes):
            v = v.encode(errors='replace')
        return v

    def __iter__(self):
        return self.key_list.__iter__()


def main(argv):

    parser = argparse.ArgumentParser(
        description='small pyexiv2 standalone parser')
    parser.add_argument('filename', type=str,
                        help='The path of the file to treat')
    parser.add_argument('--tag', type=str,
                        help='The tag to get')
    parser.add_argument('--value', type=str,
                        help='set the value of the tag')

    args = vars(parser.parse_args(argv))
    filename = args['filename']
    exiv2_metadata = ImageMetadata(filename)
    exiv2_metadata.read()

    if args['tag'] is None:
        return

    key = args['tag']
    if args['value'] is not None:
        value = args['value']
        exiv2_metadata[key] = ExifTag(key, value)
        exiv2_metadata.write()
    else:
        v = exiv2_metadata[key].value
        # except:
        #  v = exiv2_metadata[key].raw_value
        print(v)


if __name__ == '__main__':
    main(sys.argv)
