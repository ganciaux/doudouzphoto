#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
from photo_list import PhotoList
from photo_db import PhotoDB
from datetime import datetime
from photo import Photo


def main(argv):

    import argparse
    parser = argparse.ArgumentParser(
        description='Doudouz application to set date for photo collection')

    # option for the checking feature
    parser.add_argument(
        '--date', type=str, required=True,
        help='date in format "Y-m-d H:M:S"')

    parser.add_argument(
        '-f', '--force', action='store_true',
        help=('Force setting the date.'
              'Otherwise do it when there is no date set yet'))

    args = vars(parser.parse_args())

    date = args['date']
    date = datetime.strptime(date, "%Y-%m-%d %H:%M:%S")
    print(date)
    photo_list = PhotoList()
    photo_list.read_from_standard_input()
    # print(photo_list)

    db = PhotoDB(db_path=os.getcwd())

    for f in photo_list:
        photo = Photo.createPhoto(f)
        photo_proxy = db.getPhotoProxyFromHash(photo.getHash())
        if (photo.getDate() is None) or (args['force'] is True):
            print("setting date of {0}: {1}".format(
                f, date))
            photo_proxy.setDate(date)


if __name__ == '__main__':
    main(sys.argv)
