#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Module to delete duplicated photos
"""


from photo_db import PhotoDB
from photo_organizer import PhotoOrganizer
from photo_state_messager import PhotoStateMessage
import sys

# -------------------------------------------------------------------------- #


class PhotoSecureDelete(PhotoStateMessage):

    """
class PhotoSecureDelete: Documentation TODO
    """

    def __init__(self, photo_db_to_clean, photo_db_reference):
        """ Constructor """

        self.photo_db_reference = photo_db_reference
        self.photo_db_to_clean = photo_db_to_clean

        if (self.photo_db_reference.getRootPath() ==
                self.photo_db_to_clean.getRootPath()):
            raise Exception("ref and to clean are same: abort")

    def __del__(self):
        """ Destructor """
        pass

    def cleanDuplicates(self):
        """ Clean the duplicates """

        cpt = 0
        sz = len(self.photo_db_to_clean)

        for photo in self.photo_db_to_clean:
            cpt += 1
            self.setTaskCompleteness(100. * cpt / sz)

            if not self.photo_db_reference.doContainsPhoto(photo):
                continue

            fname = photo['filename']

            photo_ref = self.photo_db_reference.getPhotoProxyFromHash(
                photo['hash'])
            print("Photo Reference is: {0}".format(photo_ref['filename']))

            self.printInfo("delete permanently the photo: {0}".format(fname))
            photo.remove(deep=True)

        orga = PhotoOrganizer(self.photo_db_to_clean)
        orga.cleanEmptyDirectories()


# -------------------------------------------------------------------------- #

def main():

    db_path_to_clean = sys.argv[1]
    db_path_reference = sys.argv[2]

    if db_path_to_clean == db_path_reference:
        raise RuntimeError("ref and to clean are same: abort")

    db_reference = PhotoDB(db_path=db_path_reference)
    organizer = PhotoOrganizer(db_reference)
    organizer.photo_db.checkSanity()
    db_to_clean = PhotoDB(db_path=db_path_to_clean)
    eraser = PhotoSecureDelete(db_to_clean, db_reference)

    eraser.cleanDuplicates()


if __name__ == '__main__':
    main()
