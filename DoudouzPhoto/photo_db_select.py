#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Select the photo database
"""

from PyQt4 import QtGui, QtCore
from photo_db import PhotoDB
from photo_db_conf import PhotoDBConf
import sys
import os
import pickle

################################################################


def sizeof_fmt(num, suffix='B'):
    if num is None:
        return 'Undefined'
    for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)

################################################################


class PhotoDBSelect(QtGui.QWidget, PhotoDBConf):

    changedDB = QtCore.pyqtSignal(str)

    def __init__(self, mother=None, db_path=None):

        PhotoDBConf.__init__(self)

        QtGui.QWidget.__init__(self, mother)

        self.file_edit = QtGui.QLineEdit(self)
        self.layout = QtGui.QVBoxLayout(self)
        self.layout.addWidget(self.file_edit)
        self.button_select = QtGui.QPushButton('Select database', self)
        self.layout.addWidget(self.button_select)
        self.button_select.clicked.connect(self.select_file)

        self.nb_photo_label = QtGui.QLabel(self)
        self.layout.addWidget(self.nb_photo_label)

        self.byte_size_label = QtGui.QLabel(self)
        self.layout.addWidget(self.byte_size_label)

        self.conf_key = 'db_path'
        if db_path is not None:
            self.conf[self.conf_key] = db_path
        self.set_db_path()
        self.printDBInfo()
        self.show()

    def printDBInfo(self):
        try:
            photo_db = PhotoDB(self.conf[self.conf_key])
            nb_photos = photo_db.size()
            byte_size = sizeof_fmt(photo_db.getByteSize())
            self.nb_photo_label.setText("Number of photos: " + str(nb_photos))
            self.byte_size_label.setText(
                "Total occupied size: " + str(byte_size))

        except Exception as e:
            raise e

    def set_db_path(self):
        self.file_edit.setText(self.trUtf8(self.getDBPath()))
        self.printDBInfo()
        self.changedDB.emit(self.trUtf8(self.getDBPath()))

    def select_file(self):
        self.conf[self.conf_key] = QtGui.QFileDialog.getExistingDirectory(
            self, 'Open Directory', self.conf[self.conf_key])
        self.conf[self.conf_key] = self.conf[self.conf_key].toUtf8()
        self.conf[self.conf_key] = str(self.conf[self.conf_key])
        self.set_db_path()

    def getDBPath(self):
        if self.conf_key not in self.conf:
            self.conf[self.conf_key] = os.path.expanduser('~')

        return self.conf[self.conf_key]

################################################################


def main():

    app = QtGui.QApplication(sys.argv)
    progress = PhotoDBSelect()
    app.exec_()


if __name__ == '__main__':
    main()
