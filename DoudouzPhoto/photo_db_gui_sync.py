#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Grapical module for a list of Photos
"""

from photo import Photo
from photo_db import PhotoDB
from photo_organizer import PhotoOrganizer
from photo_db_sync import PhotoDBSynch
from photo_db_select import PhotoDBSelect
from photo_db_conf import PhotoDBConf
from photo_db_gui_action import PhotoDBActionDialog
from secureDeletePhoto import PhotoSecureDelete
from PyQt4 import QtGui, QtCore
import sys
import os
import pickle

################################################################


class PhotoDBGUISync(QtGui.QWidget, PhotoDBConf):

    def __init__(self, mother=None):
        PhotoDBConf.__init__(self)
        QtGui.QWidget.__init__(self, mother)

        self.mother = mother
        self.dst_base_label = QtGui.QLabel('DataBase where to Backup', self)
        self.dst_base_selector = PhotoDBSelect()
        self.dst_base_selector.changedDB.connect(self.setDstDBPath)
        self.dst_base_selector.conf_key = 'db_backup'
        self.dst_base_selector.set_db_path()

        self.button_sync = QtGui.QPushButton('Sync/Backup', self)
        self.button_clean = QtGui.QPushButton('Secure Delete', self)
        self.layout = QtGui.QVBoxLayout(self)
        self.setLayout(self.layout)

        self.layout.addWidget(self.dst_base_label)
        self.layout.addWidget(self.dst_base_selector)
        self.layout.addWidget(self.button_sync)
        self.layout.addWidget(self.button_clean)

        self.button_sync.clicked.connect(self.sync)
        self.button_clean.clicked.connect(self.clean)

        self.show()

    def sync(self):

        def _action(printInfo, setPercent, exceptionSignal):
            db_src = PhotoDB(db_path=self.db_path)
            db_src.printInfo = printInfo
            db_dst = PhotoDB(db_path=self.dst_db_path)
            db_dst.printInfo = printInfo
            synch = PhotoDBSynch(db_src, db_dst)
            synch.printInfo = printInfo
            synch.setTaskCompleteness = setPercent

            synch.synch()
            printInfo('Done Synch', True)

        progress = PhotoDBActionDialog(progress=True)
        progress.finished.connect(self.dst_base_selector.set_db_path)
        progress.exec_("Synchronization in progress", _action)

    def clean(self):
        def _action(printInfo, setPercent, exceptionSignal):

            db_src = PhotoDB(db_path=self.db_path)
            db_src.printInfo = printInfo
            db_dst = PhotoDB(db_path=self.dst_db_path)
            db_dst.printInfo = printInfo
            eraser = PhotoSecureDelete(db_src, db_dst)
            eraser.printInfo = printInfo
            eraser.setTaskCompleteness = setPercent

            eraser.cleanDuplicates(True)
            printInfo('Done Cleaning', True)

        msgBox = QtGui.QMessageBox()
        msgBox.setText(
            "Are you sure you want to delete permanently the photos?")
        msgBox.setText("The concerned database is {0}".format(
            self.trUtf8(self.db_path)))
        msgBox.setStandardButtons(QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
        msgBox.setDefaultButton(QtGui.QMessageBox.No)
        ret = msgBox.exec_()
        if ret == QtGui.QMessageBox.No:
            return

        progress = PhotoDBActionDialog(progress=True)
        progress.finished.connect(self.mother.base_selector.set_db_path)
        progress.exec_("Synchronization in progress", _action)

    def setDstDBPath(self, path):
        self.dst_db_path = str(path)

    def setDBPath(self, path):
        self.db_path = str(path.toUtf8())


################################################################

def main():

    app = QtGui.QApplication(sys.argv)
    base_selector = PhotoDBSelect()

    sync = PhotoDBGUISync()

    base_selector.changedDB.connect(sync.setDBPath)
    base_selector.set_db_path()
    app.exec_()


if __name__ == '__main__':
    main()
