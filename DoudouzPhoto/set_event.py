#!/usr/bin/env python3
################################################################
import sys
import os
from DoudouzPhoto.photo import Photo
from DoudouzPhoto.photo_list import PhotoList
from DoudouzPhoto.photo_db import PhotoDB
################################################################

photo_db = PhotoDB(db_path=os.getcwd())
photo_list = PhotoList()
photo_list.read_from_standard_input()

print('done')

for fname in photo_list:

    events = sys.argv[1:]
    photo = Photo.createPhoto(fname)
    photo_hash = photo.getHash()
    photo_proxy = photo_db.getPhotoProxyFromHash(photo_hash)
    print("setting event to " + photo.getFilename() + "|" + str(events))
    photo_proxy.setEvents(events)
