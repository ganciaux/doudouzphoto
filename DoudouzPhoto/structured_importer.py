#!/usr/bin/env python3
# -*- coding: utf-8 -*-
################################################################
# parse parameters
import sys
import os
import guess_date


def structured_import(directory_to_import, events):

    # first build the local database and fill it
    import photo_db
    db_src = photo_db.PhotoDB(db_path=directory_to_import)
    db_src.scanPhotos(recompute_hash=False)
    del db_src

    # check that all photos have a correct date
    db_src = photo_db.PhotoDB(db_path=directory_to_import)
    for photo in db_src:
        date = photo.getDate()
        fname = photo.getFilename()
        print(fname)
        if date is None:
            date = guess_date.guessDate(fname)
        # if date is None:
        #    raise Exception('{0}: has no date -> abort import'.format(fname))

    # set the event

    if events is None:
        events = []
    if type(events) == str:
        events = events.split(' ')

    for photo in db_src:
        fname = db_src.getRelativePath(photo)
        dir_name = os.path.dirname(fname)
        _events = events + dir_name.split('/')
        _events = [e for e in _events if not e == '']
        print(photo.getFilename() + ": setting event to " + str(_events))
        db_src.setEvents(photo, _events)

    # rescan for events
    db_src.scanPhotos(recompute_hash=False)

    # organize the local base
    import photo_organizer
    organizer = photo_organizer.PhotoOrganizer(db_src)
    organizer.reorganize()
    organizer.cleanEmptyDirectories()


def main():

    directory_to_import = sys.argv[1]
    events = None
    if len(sys.argv) > 2:
        events = sys.argv[2:]
    else:
        events = None
    structured_import(directory_to_import, events)


if __name__ == '__main__':
    main()
