#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Module containing class MetaDataManagerInterface
"""

# -------------------------------------------------------------------------- #


class MetaDataManagerInterface:

    """
 class MetaDataManager: Documentation TODO
    """

    # ------------------------------------------------------------------ #
    # Constructors/Destructors                                           #
    # ------------------------------------------------------------------ #

    def __init__(self, filename):
        """ Constructor """
        raise Exception('pure virtual function')

    def setTag(self, key, value):
        """ set a tag """
        raise Exception('pure virtual function')

    def getTag(self, key):
        """ set a tag """
        raise Exception('pure virtual function')

    def __iter__(self):
        raise Exception('pure virtual function')
