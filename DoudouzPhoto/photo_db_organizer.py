#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Grapical module for scanning a directory
"""

from photo_db import PhotoDB
from photo_organizer import PhotoOrganizer
from photo_db_select import PhotoDBSelect
from photo_db_gui_action import PhotoDBActionDialog
from PyQt4 import QtGui, QtCore
import sys
import os

################################################################


class PhotoDBOrganizer(QtGui.QWidget):

    def __init__(self, mother):
        QtGui.QWidget.__init__(self, mother)

        self.mother = mother
        self.button_organize = QtGui.QPushButton('Organize', self)
        self.button_flat = QtGui.QPushButton('Flatten', self)
        self.layout = QtGui.QVBoxLayout(self)
#        self.terminal = QtGui.QPlainTextEdit(self)
#        self.terminal.setReadOnly(True)

        self.setLayout(self.layout)
        self.layout.addWidget(self.button_organize)
        self.layout.addWidget(self.button_flat)
#        self.layout.addWidget(self.terminal)
        self.button_organize.clicked.connect(self.organize)
        self.button_flat.clicked.connect(self.organizeFlat)
        self.show()

    def setDBPath(self, path):
        self.db_path = str(path.toUtf8())

    def action(self, printInfo, setPercent, exceptionSignal):
        db = PhotoDB(db_path=self.db_path)
        db.printInfo = printInfo
        organizer = PhotoOrganizer(db)
        organizer.setTaskCompleteness = setPercent
        organizer.printInfo = printInfo
        organizer.checkSanity()
        if self.flat is True:
            organizer.flatten()
        else:
            organizer.reorganize()
        organizer.cleanEmptyDirectories()
        printInfo('Done Organization', True)

    def organizeFlat(self):
        self.flat = True
        progress = PhotoDBActionDialog(progress=True)
        progress.finished.connect(self.mother.base_selector.set_db_path)
        progress.exec_("Organization in progress", self.action)

    def organize(self):
        self.flat = False
        progress = PhotoDBActionDialog(progress=True)
        progress.finished.connect(self.mother.base_selector.set_db_path)
        progress.exec_("Organization in progress", self.action)


################################################################

def main():

    app = QtGui.QApplication(sys.argv)
    organizer = PhotoDBOrganizer()
    base_selector = PhotoDBSelect()
    base_selector.changedDB.connect(organizer.setDBPath)
    base_selector.set_db_path()
    app.exec_()


if __name__ == '__main__':
    main()
