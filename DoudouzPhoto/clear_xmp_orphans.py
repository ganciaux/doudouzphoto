import os
import re

for root, dummy_dirs, files in os.walk(os.getcwd()):

    for _file in files:

        dummy_base, ext = os.path.splitext(_file)
        if ext != '.xmp':
            continue

        full_name = os.path.join(root, _file)
        image_name = os.path.join(root, dummy_base)
        if not os.path.exists(image_name):
            print("remove orphan:", full_name)
            os.remove(full_name)
