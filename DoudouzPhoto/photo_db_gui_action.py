#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Dialog box with progress bar for actions to be run
"""

from PyQt4 import QtGui, QtCore
import sys
from photo_db import PhotoDB
################################################################


class DialogWorker(QtCore.QThread):

    printInfoSignal = QtCore.pyqtSignal(str)
    setPercentSignal = QtCore.pyqtSignal(float)
    exceptionSignal = QtCore.pyqtSignal(Exception)

    def __init__(self, commander, action):
        QtCore.QThread.__init__(self)
        self.commander = commander
        self.msg_counter = 0
        self.mesg = ""
        self.action = action

    def run(self):
        try:
            self.action(self.printInfo, self.setPercent, self.exceptionSignal)
        except Exception as e:
            self.exceptionSignal.emit(e)

    def printInfo(self, mesg, flush=False):
        if mesg is None:
            return

        self.msg_counter += 1
        self.mesg += str(mesg) + "\n"

        if self.msg_counter > 20 or flush is True:
            self.msg_counter = 0
            self.printInfoSignal.emit(self.mesg)
            self.mesg = ""

    def setPercent(self, percent):
        self.setPercentSignal.emit(percent)


################################################################


class PhotoDBActionDialog(QtGui.QDialog):

    def __init__(self, progress=False, auto_close=False, terminal=True):
        QtGui.QDialog.__init__(self)
        self.message_label = QtGui.QLabel(self)
        self.progress = QtGui.QProgressBar(self)
        self.terminal = QtGui.QPlainTextEdit(self)
        self.terminal.setReadOnly(True)
        self.button_close = QtGui.QPushButton('Abort', self)
        self.button_close.clicked.connect(self.close_button)

        self.layout = QtGui.QVBoxLayout(self)
        self.setLayout(self.layout)
        self.layout.addWidget(self.message_label)
        self.layout.addWidget(self.progress)
        self.layout.addWidget(self.terminal)
        self.layout.addWidget(self.button_close)

        self.auto_close = auto_close
        if progress is False:
            self.progress.hide()
        if terminal is False:
            self.terminal.hide()

    def close_button(self):
        do_close = True
        if not self.dialog_worker.isFinished():
            msgBox = QtGui.QMessageBox()
            msgBox.setText("Are you sure you want to cancel ?")
            msgBox.setText("In general it is not a good idead")
            msgBox.setStandardButtons(
                QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
            msgBox.setDefaultButton(QtGui.QMessageBox.No)
            ret = msgBox.exec_()
            if ret == QtGui.QMessageBox.No:
                do_close = False
            if ret == QtGui.QMessageBox.Yes:
                do_close = True

        if do_close:
            self.dialog_worker.terminate()
            self.done(-1)

    def exec_(self, message, action):
        self.setWindowTitle('Please wait')

        self.message_label.setText(message)
        self.button_close.setText('Abort')
        self.dialog_worker = DialogWorker(self, action)
        self.dialog_worker.finished.connect(self.workIsDone)
        self.dialog_worker.printInfoSignal.connect(self.printInfo)
        self.dialog_worker.setPercentSignal.connect(self.setPercent)
        self.dialog_worker.exceptionSignal.connect(self.exceptionHandler)
        self.dialog_worker.start()
        QtGui.QDialog.exec_(self)

    def exceptionHandler(self, e):
        error_message = QtGui.QErrorMessage(self)
        error_message.showMessage(str(e))
        raise e

    def workIsDone(self):
        if self.auto_close:
            self.done(0)
        else:
            self.button_close.setText('Close')

    def printInfo(self, mesg):
        if mesg is None:
            return
        self.terminal.insertPlainText(self.trUtf8(mesg))
        self.terminal.ensureCursorVisible()

    def setPercent(self, percent):
        self.progress.setValue(percent)


################################################################
# example of usage
################################################################

def custom_action(printInfo, setPercent):
    db_path = '/home/tarantino/sorted'
    db = PhotoDB(db_path)
    db.printInfo = printInfo
    db.scanPhotos()
    printInfo('Done Scanning', True)


def main():

    app = QtGui.QApplication(sys.argv)
    progress = PhotoDBActionDialog(progress=True)
    progress.exec_("message", custom_action)
    # app.exec_()


if __name__ == '__main__':
    main()
