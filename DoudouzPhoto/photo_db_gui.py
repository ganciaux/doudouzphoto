#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Grapical module for PhotoDB app
"""


from photo_db import PhotoDB
from photo_organizer import PhotoOrganizer
from photo_db_sync import PhotoDBSynch
from photo_db_organizer import PhotoDBOrganizer
from photo_db_select import PhotoDBSelect
from photo_db_scan import PhotoDBScan
from photo_db_gui_sync import PhotoDBGUISync
from photo_db_gui_photo_selector import PhotoDBPhotoSelector

from PyQt4 import QtGui, QtCore
import sys
import os
import pickle

################################################################


class PhotoDBSelector(QtGui.QWidget):

    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.base_selector = PhotoDBSelectBase(self)
        self.date_start = QtGui.QDateEdit(QtCore.QDate(1900, 1, 1), self)
        self.date_start.setCalendarPopup(True)
        self.date_start_label = QtGui.QLabel(self)
        self.date_start_label.setText("Date Start Selection")
        self.date_end = QtGui.QDateEdit(QtCore.QDate(2100, 1, 1), self)
        self.date_end.setCalendarPopup(True)
        self.date_end_label = QtGui.QLabel(self)
        self.date_end_label.setText("Date End Selection")
        self.event = QtGui.QComboBox(self)
        self.event_label = QtGui.QLabel(self)
        self.event_label.setText("Event Selection")
        self.button_select = QtGui.QPushButton('Select photos', self)
        self.list_photos = QtGui.QListWidget(self)
        self.button_extract = QtGui.QPushButton('Extract Photos', self)

        self.layout = QtGui.QVBoxLayout(self)
        self.setLayout(self.layout)
        self.layout.addWidget(self.base_selector)
        self.layout.addWidget(self.date_start_label)
        self.layout.addWidget(self.date_start)
        self.layout.addWidget(self.date_end_label)
        self.layout.addWidget(self.date_end)
        self.layout.addWidget(self.event_label)
        self.layout.addWidget(self.event)
        self.layout.addWidget(self.button_select)
        self.layout.addWidget(self.list_photos)
        self.layout.addWidget(self.button_extract)

        self.base_selector.setDBPathSignal.connect(self.updateCombo)
        self.button_select.clicked.connect(self.fetchPhotoList)
        self.button_extract.clicked.connect(self.extract)
        self.updateCombo()

        self.show()

    def extract(self):
        db_path_src = self.base_selector.getDBPath()
        db_path_dst = str(
            QtGui.QFileDialog.getExistingDirectory(self, 'Open Directory'))
        db_src = PhotoDB(db_path=db_path_src)
        db_dst = PhotoDB(db_path=db_path_dst)
        synch = PhotoDBSynch(db_src, db_dst)
        print self.photo_hash_list
        synch.synchFromHashList(self.photo_hash_list)

    def fetchPhotoList(self):
        db_path = self.base_selector.getDBPath()
        db = PhotoDB(db_path)

        start_date = self.getStartDate()
        end_date = self.getEndDate()
        event = self.getEvent()
        self.list_photos.clear()
        self.photo_hash_list = []
        for photo_hash in db.iterateHash(start_date=start_date,
                                         end_date=end_date,
                                         event=event):
            photo = db.getPhotoProxyFromHash(photo_hash)
            rel_fname = photo.getRelativePath()
            creation_date = photo['creation_date']
            self.list_photos.insertItem(
                0, str(creation_date) + "\t" + self.trUtf8(rel_fname))
            self.photo_hash_list.append(photo_hash)

    def getStartDate(self):
        d = self.date_start.dateTime().toPyDateTime()
        return d

    def getEndDate(self):
        d = self.date_end.dateTime().toPyDateTime()
        return d

    def getEvent(self):
        e = unicode(self.event.currentText()).encode('utf8')
        if e == '':
            return None
        return e

    def updateCombo(self):
        db_path = self.base_selector.getDBPath()
        db = PhotoDB(db_path)
        events = db.getAllEvents()
        events = [self.trUtf8(e) for e in events]
        self.event.clear()
        self.event.insertItems(0, events)
        index = self.event.findText('')
        self.event.setCurrentIndex(index)

################################################################


class WorkerScan(QtCore.QThread):

    printInfoSignal = QtCore.pyqtSignal(str)
    setPercentSignal = QtCore.pyqtSignal(float)

    def __init__(self, commander):
        QtCore.QThread.__init__(self)
        self.commander = commander
        self.msg_counter = 0
        self.mesg = ""

    def run(self):
        if self.task == 'scan':
            self.scan()
        if self.task == 'organize':
            self.organize()

    def scan(self):
        db_path = self.commander.base_selector.getDBPath()
        db = PhotoDB(db_path=db_path)
        db.printInfo = self.printInfo
        db.scanPhotos()
        self.printInfo('Done Scanning', True)

    def organize(self):
        self.db_path = self.commander.base_selector.getDBPath()
        db = PhotoDB(db_path=self.db_path)
        db.printInfo = self.printInfo
        organizer = PhotoOrganizer(db)
        organizer.setTaskCompleteness = self.setPercent
        organizer.printInfo = self.printInfo
        organizer.checkSanity()
        organizer.reorganize()
        organizer.cleanEmptyDirectories()
        self.printInfo('Done Organization', True)

    def setTask(self, task):
        self.task = task

    def printInfo(self, mesg, flush=False):
        if mesg is None:
            return

        self.msg_counter += 1
        self.mesg += mesg + "\n"

        if self.msg_counter > 20 or flush is True:
            self.msg_counter = 0
            self.printInfoSignal.emit(self.mesg)
            self.mesg = ""

    def setPercent(self, percent):
        self.setPercentSignal.emit(percent)


################################################################
class PhotoDBWindowProgress(QtGui.QWidget):

    def __init__(self):
        QtGui.QWidget.__init__(self)

        self.progress = QtGui.QProgressBar(self)
        self.button_organize = QtGui.QPushButton('Organize', self)
        self.button_scan = QtGui.QPushButton('Scan', self)
        self.layout = QtGui.QVBoxLayout(self)
        self.terminal = QtGui.QPlainTextEdit(self)
        self.terminal.setReadOnly(True)
        self.base_selector = PhotoDBSelectBase(self)

        self.setLayout(self.layout)
        self.layout.addWidget(self.base_selector)
        self.layout.addWidget(self.button_organize)
        self.layout.addWidget(self.button_scan)
        self.layout.addWidget(self.progress)
        self.layout.addWidget(self.terminal)
        self.button_organize.clicked.connect(self.organize)
        self.button_scan.clicked.connect(self.scan)
        self.show()
        self.scan_worker = WorkerScan(self)
        self.scan_worker.printInfoSignal.connect(self.printInfo)
        self.scan_worker.setPercentSignal.connect(self.setPercent)

    def printInfo(self, mesg):
        if mesg is None:
            return
        self.terminal.insertPlainText(self.trUtf8(mesg))
        self.terminal.ensureCursorVisible()

    def setPercent(self, percent):
        self.progress.setValue(percent)

    def customEvent(self, event):
        if event.type() == 'printInfo':
            self.printInfo(event.data())

    def organize(self):
        self.terminal.clear()
        self.scan_worker.setTask('organize')
        self.scan_worker.start()

    def scan(self):
        self.terminal.clear()
        self.scan_worker.setTask('scan')
        self.scan_worker.start()


################################################################

class PhotoDBMainWindow(QtGui.QMainWindow):
    def __init__(self, db_path=None):
        QtGui.QMainWindow.__init__(self)

        import locale
        locale.setlocale(locale.LC_ALL, "C")

        self.scanner = PhotoDBScan(self)
        self.selector = PhotoDBPhotoSelector(self)
        self.organizer = PhotoDBOrganizer(self)
        self.synch = PhotoDBGUISync(self)
        self.base_selector = PhotoDBSelect(self, db_path)

        self.base_selector.changedDB.connect(self.scanner.setDBPath)
        self.base_selector.changedDB.connect(self.organizer.setDBPath)
        self.base_selector.changedDB.connect(self.selector.setDBPath)
        self.base_selector.changedDB.connect(self.synch.setDBPath)

        self.setWindowTitle('Doudouz photo organizer')

        self.tabs = QtGui.QTabWidget()
        self.tabs.addTab(self.scanner, 'Sanner')
        self.tabs.addTab(self.organizer, 'Organizer')
        self.tabs.addTab(self.selector, 'Selector')
        self.tabs.addTab(self.synch, 'Backup/Eraser')

        self.central_widget = QtGui.QWidget(self)
        self.setCentralWidget(self.central_widget)

        self.layout = QtGui.QVBoxLayout(self.central_widget)
        self.layout.addWidget(self.base_selector)
        self.layout.addWidget(self.tabs)

        self.base_selector.set_db_path()
        self.show()

    def __del__(self):
        print "AAAAAAAA"
        self.base_selector.__del__()


################################################################


def main():

    if len(sys.argv) > 1:
        db_path = sys.argv[1]
    else:
        db_path = None
    app = QtGui.QApplication(sys.argv)
    progress = PhotoDBMainWindow(db_path=db_path)
    app.exec_()
    progress.__del__()


if __name__ == '__main__':
    main()
