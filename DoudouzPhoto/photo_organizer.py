#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Module for reorganizing files
"""


import os
import sys
from .photo_db import PhotoDB
from .photo_state_messager import PhotoStateMessage
from .photo_iterator import PhotoIterator

# -------------------------------------------------------------------------- #


class PhotoOrganizer(PhotoStateMessage):

    def __init__(self, photo_db):
        """ Constructor of the reorganizer """

        self.photo_db = photo_db

    def __del__(self):
        """ Destructor: close the organizer """
        pass

    def displaceFile(self, photo):
        new_fname = photo.getYearMonthEventFilePath()
        if new_fname is None:
            raise Exception('internal error')
        photo.move(new_fname)

    def lowerCaseFile(self, photo):
        new_fname = self.photo_db.getRelativePath(photo)
        # print new_fname
        new_fname = new_fname.lower()
        # print new_fname
        self.photo_db.move(photo, new_fname)

    def isInSubdir(self, filename, directory):

        path = filename
        basename = os.path.basename(path)
        path = os.path.realpath(path)
        directory = os.path.realpath(directory)
        guess_path = os.path.join(directory, basename)
        if guess_path == path:
            return True
        return False

    def lowerCase(self, subdir=None):
        """ Lower case all the file names """

        cpt = 0
        sz = len(self.photo_db)
        freq = sz / 20
        if freq == 0:
            freq = 1
        for photo in self.photo_db:
            cpt += 1
            if (cpt % freq == 0):
                sys.stderr.write('.')
            self.setTaskCompleteness(100. * cpt / sz)
            if subdir is None or self.isInSubdir(photo, subdir):
                self.lowerCaseFile(photo)

        self.setTaskCompleteness(100)
        print('')

    def reorganize(self, subdir=None):
        """ Displace files in order to make a consistent tree """

        cpt = 0
        sz = len(self.photo_db)
        freq = sz / 20
        if freq == 0:
            freq = 1
        photo_entries = PhotoIterator(self.photo_db, it_entries=True)
        for photo in photo_entries:
            cpt += 1
            if (cpt % freq == 0):
                sys.stderr.write('.')
            self.setTaskCompleteness(100. * cpt / sz)
            if subdir is None or self.isInSubdir(photo['filename'], subdir):
                try:
                    self.displaceFile(photo)
                except Exception as e:
                    print(e)

        self.setTaskCompleteness(100)
        print('')

    def reorganizeAll(self):
        """ Displace files in order to make a consistent tree """

        cpt = 0
        sz = len(self.photo_db)
        freq = sz / 20
        if freq == 0:
            freq = 1
        for photo in self.photo_db:
            cpt += 1
            if (cpt % freq == 0):
                sys.stderr.write('.')
            self.setTaskCompleteness(100. * cpt / sz)
            self.displaceFile(photo)

        self.setTaskCompleteness(100)
        print('')

    def flatten(self):
        """ Displace files in order to make them all in the same directory """

        cpt = 0
        sz = len(self.photo_db)
        freq = sz / 20
        if freq == 0:
            freq = 1
        for photo in self.photo_db:
            cpt += 1
            if (cpt % freq == 0):
                sys.stderr.write('.')
            self.setTaskCompleteness(100. * cpt / sz)
            new_fname = self.photo_db.getRelativePath(photo)
            new_fname = os.path.basename(new_fname)
            new_fname = os.path.join(self.photo_db.getRootPath(), new_fname)
            #            print new_fname
            if new_fname is None:
                raise Exception('internal error')
            if new_fname is None:
                continue
            self.photo_db.move(photo, new_fname)

        self.setTaskCompleteness(100)
        print('')

    def cleanEmptyDirectories(self):
        """ Remove all empty directories """
        root_path = self.photo_db.getRootPath()
        for root, dirs, files in os.walk(root_path):
            for d in dirs:
                try:
                    dd = os.path.join(root, d)
                    os.removedirs(dd)
                    self.printInfo("cleared " + d)
                except Exception as e:
                    # print e
                    pass

        self.photo_db.commit()

# -------------------------------------------------------------------------- ##


def main():

    import argparse
    parser = argparse.ArgumentParser(
        description='Doudouz application for photo organizer')
    parser.add_argument('db_path', type=str,
                        help='The path of the database to treat')

    # option for the scanning feature
    parser.add_argument('--organize', action='store_true',
                        help='request the database to be organized')
    parser.add_argument('--lower_case', action='store_true',
                        help='request the database to lower case all files')
    parser.add_argument('--flatten', action='store_true',
                        help='request the database to be flattened')
    parser.add_argument('--subdir', type=str, help='acts on a subdir only')

    args = vars(parser.parse_args())
    db_path = args['db_path']
    db = PhotoDB(db_path=db_path)

    organizer = PhotoOrganizer(db)
    if args['organize'] is True:
        db.checkSanity()
        organizer.reorganize(subdir=args['subdir'])

    if args['flatten'] is True:
        organizer.flatten()

    if args['lower_case'] is True:
        if 'subdir' in args:
            organizer.lowerCase(subdir=args['subdir'])
        else:
            organizer.lowerCase()

    organizer.cleanEmptyDirectories()
    print("Done")


if __name__ == '__main__':
    main()
