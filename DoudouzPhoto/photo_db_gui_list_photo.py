#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Grapical module for a list of Photos
"""

from photo import Photo
from photo_db import PhotoDB
from photo_organizer import PhotoOrganizer
from photo_db_sync import PhotoDBSynch
from photo_db_select import PhotoDBSelect
from photo_db_gui_action import PhotoDBActionDialog
from PyQt4 import QtGui, QtCore
import sys
import os
import pickle

################################################################


class PhotoDBListPhoto(QtGui.QWidget):

    def __init__(self, mother):
        QtGui.QWidget.__init__(self, mother)
        self.image_label = QtGui.QLabel(self)
        self.image_label.setText('')
        self.list_photos = QtGui.QTableWidget(self)
        self.list_photos.currentItemChanged.connect(self.currentPhotoChanged)

        self.layout = QtGui.QHBoxLayout(self)
        self.setLayout(self.layout)
        self.layout.addWidget(self.list_photos)
        self.layout.addWidget(self.image_label)

        self.selected_photo_hash = None
        self.show()

    def currentPhotoChanged(self, entry_selected):
        row_number = entry_selected.row()
        photo_hash = self.photo_list[row_number]
        self.selected_photo_hash = photo_hash
        photo_db = PhotoDB(self.db_path)
        photo = photo_db.getPhotoProxyFromHash(photo_hash)
        fname = photo['filename']
        self.image = QtGui.QPixmap(fname)
        self.image = self.image.scaledToHeight(300)
        self.image_label.setPixmap(self.image)

    def getSelectedPhotoHash(self):
        return self.selected_photo_hash

    def setList(self, photo_list, db_path):
        self.photo_list = photo_list
        self.db_path = db_path
        progress = PhotoDBActionDialog(
            progress=True, auto_close=True, terminal=False)
        progress.exec_("Fectching Photo Information", self.setListAction)

    def setListAction(self, printInfo, setPercent, exceptionSignal):
        self.list_photos.clear()
        photo_db = PhotoDB(self.db_path)

        self.list_photos.setColumnCount(4)
        self.list_photos.setHorizontalHeaderLabels(
            ['event', 'filename', 'date', 'hash'])
        self.list_photos.verticalHeader().hide()
        self.list_photos.setSelectionBehavior(
            QtGui.QAbstractItemView.SelectRows)
        self.list_photos.setSelectionMode(
            QtGui.QAbstractItemView.SingleSelection)
#        self.list_photos.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)

        cpt = 0
        self.list_photos.setRowCount(len(self.photo_list))
        print(self.list_photos.rowCount())
        sz = len(self.photo_list)
        for photo_hash in self.photo_list:
            setPercent(100. * cpt / sz)
            try:
                photo = photo_db.getPhotoProxyFromHash(photo_hash)
                rel_fname = photo.getRelativePath()
                events = photo.getEvents()
                if events == [None]:
                    events = 'None'
                if type(events) == list:
                    events = "/".join(events)
                if events is None:
                    events = 'None'
                creation_date = photo['creation_date']

                self.list_photos.setItem(
                    cpt, 0, QtGui.QTableWidgetItem(self.trUtf8(events)))
                self.list_photos.setItem(
                    cpt, 1, QtGui.QTableWidgetItem(self.trUtf8(rel_fname)))
                self.list_photos.setItem(
                    cpt, 2, QtGui.QTableWidgetItem(str(creation_date)))
                self.list_photos.setItem(
                    cpt, 3, QtGui.QTableWidgetItem(photo_hash))
            except Exception as e:
                exceptionSignal.emit(e)
            cpt += 1

        print cpt
        print(self.list_photos.rowCount())


################################################################

def listPhotos(photo_db):
    photo_db = PhotoDB(str(photo_db))
    list_photo = PhotoDBListPhoto()
    photo_hashes = [h for h in photo_db.iterateHash()]
    list_photo.setList(photo_hashes, photo_db)
    raw_input('toto')


def main():

    app = QtGui.QApplication(sys.argv)
    base_selector = PhotoDBSelect()
    base_selector.changedDB.connect(listPhotos)
    base_selector.set_db_path()
    app.exec_()


if __name__ == '__main__':
    main()
