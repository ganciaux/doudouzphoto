#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Module for synchronizing two database
"""
import os
import sys

from photo_db import PhotoDB, PhotoList
from photo_state_messager import PhotoStateMessage

# -------------------------------------------------------------------------- ##


class PhotoDBSynch(PhotoStateMessage):
    # ------------------------------------------------------------------ ##
    # Constructors/Destructors                                           ##
    # ------------------------------------------------------------------ ##

    def __init__(self, photo_db_src, photo_db_dst):
        """ Constructor of the synchronizer """

        # Members ---------------------- ##

        self.photo_db_src = photo_db_src
        self.photo_db_dst = photo_db_dst

    def __del__(self):
        """ Destructor: close the synchronizer """
        pass

    # ------------------------------------------------------------------ ##
    # Methods                                                            ##
    # ------------------------------------------------------------------ ##

    def synch(self, ignore_undated=True):
        """ Displace files in order to make a consistent tree """

        h_list_src = []

        self.printInfo("Prepare file list")
        h_list_src = [h for h in self.photo_db_src.iterateHash()]
        h_list_dst = [h for h in self.photo_db_dst.iterateHash()]
        h_list = set(h_list_src) - set(h_list_dst)
        h_list = list(h_list)

        self.printInfo(
            "The plan is to sync {0} photos".format(len(h_list)))

        self.synchFromHashList(
            h_list, ignore_undated=ignore_undated)

    def synchFromHashList(self,
                          photo_hash_list, ignore_undated=True):
        """ Displace files in order to make a consistent tree """

        cpt = 0
        sz = len(photo_hash_list)
        # self.printInfo(sz)

        for photo_hash in photo_hash_list:
            cpt += 1
            self.setTaskCompleteness(100. * cpt / sz)
            try:
                photo = self.photo_db_src.getPhotoProxyFromHash(photo_hash)
            except Exception as e:
                # print(e)
                continue
            if ignore_undated is True and photo.getDate() is None:
                continue

            rel_fname = photo.getRelativePath()
            # self.printInfo(rel_fname)

            if self.photo_db_dst.doContainsFromHashPhoto(photo_hash):
                self.photo_db_dst.updatePhoto(photo)
                continue
            # fname = photo.getRelativePath()
            new_name = os.path.join(self.photo_db_dst.getRootPath(), rel_fname)

            try:
                self.printInfo("[{0}%] {1:.6}\t{3} -> {4}\t{2}".format(
                    int(100. * cpt / sz), photo_hash, rel_fname,
                    self.photo_db_src._root_path,
                    self.photo_db_dst._root_path))

                new_photo = photo.copy(new_name)
                self.photo_db_dst.add(new_photo)
            except Exception as e:
                print(e)
                print("problem while copying photo {0} -> {1}".format(
                    photo['filename'],
                    new_name))

            if cpt % 100 == 0:
                self.photo_db_dst.commit()

        self.photo_db_dst.commit()

    def copyPhoto(self):
        pass

# ------------------------------------------------------------ ##


def main(argv):

    import argparse
    parser = argparse.ArgumentParser(
        description='Doudouz application for photo synchronization')
    parser.add_argument('db_path_src', type=str,
                        help='The path of the database source')
    parser.add_argument('db_path_dst', type=str,
                        help='The path of the database destination')

    # option for treating the undated
    parser.add_argument('--treat_undated', action='store_true',
                        help='request to treat the undated ones')

    args = vars(parser.parse_args())

    db_path_src = args['db_path_src']
    db_path_dst = args['db_path_dst']

    db_src = PhotoDB(db_path=db_path_src)
    db_dst = PhotoDB(db_path=db_path_dst)

    ignore_undated = not args['treat_undated']

    synch = PhotoDBSynch(db_src, db_dst)
    synch.synch(ignore_undated=ignore_undated)

################################################################


if __name__ == '__main__':
    main(sys.argv)
