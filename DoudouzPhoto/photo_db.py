#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------- #
"""
Module containing class PhotoDB
"""
# ----------------------------------------------------------- #
import os
import sys
import sqlite3
# ----------------------------------------------------------- #
from DoudouzPhoto.photo import Photo, NotExistingFile
from DoudouzPhoto.photo_iterator import PhotoIterator
from DoudouzPhoto.photo_state_messager import PhotoStateMessage
from DoudouzPhoto.photo_proxy import PhotoProxy
from DoudouzPhoto.photo_list import PhotoList
# ----------------------------------------------------------- #


class PhotoDB(PhotoStateMessage):

    """
class PhotoDB: Documentation TODO
    """

    # ------------------------------------------------------ #
    # Constructors/Destructors                               #
    # ------------------------------------------------------ #

    def __init__(self, db_path=None, check_db_path=True):
        """ Constructor of the database """

        db_file = os.path.join(db_path, "DoudouzPhoto.db")
        if check_db_path:
            self.check_db_path(db_path)
        self._conn_db = None
        self.initSQLlite(db_file)
        self._root_path = os.path.abspath(db_path)
        self.printInfo('Opening Database: {0}'.format(self._root_path))

    def __del__(self):
        """ Destructor: close the database """
        self._conn_db.close()

    # ------------------------------------------------------ #
    # Methods                                                #
    # ------------------------------------------------------ #

    def check_db_path(self, db_path):
        tmp = os.path.join(db_path, 'DoudouzPhoto.db')
        if os.path.exists(tmp):
            return

        # print ('aaaa')
        abs_path = os.path.abspath(db_path)
        head, tail = os.path.split(abs_path)
        while (head != '') and (head != '/'):
            tmp = os.path.join(head, 'DoudouzPhoto.db')
            # print(tmp)
            if os.path.exists(tmp):
                raise Exception('Found upper directory with database: '
                                '{0}'.format(tmp))

            head, tail = os.path.split(head)

    def fetch_results(self):
        """ return the database cursor """
        return self._cursor_db.fetchall()

    def fetch_one_result(self):
        """ return the database cursor """
        return self._cursor_db.fetchone()

    def getPhotoProxyFromHash(self, _hash):
        """ Get a photo from a hash """
        return PhotoProxy(_hash, self)

    def getHashFromFilename(self, _filename):
        """ Get a photo from filename """

        request = "SELECT hash FROM photos WHERE filename = '{0}'".format(
            _filename)
        self.execute(request)
        self.entries = self.fetch_one_result()
        try:
            _hash = self.entries[0]
        except Exception as e:
            print(e)
            raise RuntimeError(
                '{0}: not part of the database'.format(_filename))
        return _hash

    def getAllEvents(self):
        """ Return a list of all registered events """
        self.execute("""SELECT eventname FROM events ORDER BY eventname""")
        events = [h[0] for h in self._cursor_db.fetchall()]
        return events

    def addToTrashTable(self, photo_hash):
        try:
            self.execute("INSERT INTO trashBin VALUES ('{0}')", photo_hash)
        except Exception as e:
            print(e)

    def getRootPath(self):
        """ return the root path """
        return self._root_path

    def isPhotoInTrash(self, _hash):
        """ check if a photo is in trash """

        self.execute(
            "SELECT count(photoid) FROM trashBin WHERE photoid='{0}'".format(
                _hash))
        item = self._cursor_db.fetchone()
        is_in_trash = item[0]
        if is_in_trash == 0:
            return False
        elif is_in_trash > 1:
            raise Exception('under construction')
        return True

    def removePhotoRegisteredInTrash(self):
        trash_photos = PhotoIterator(self, it_trash=True)
        for h in trash_photos:
            try:
                if self.doContainsFromHashPhoto(h):
                    photo = self.getPhotoProxyFromHash(h)
                    photo.remove()
            except Exception as e:
                raise e

            try:
                self.removeFromHash(h)
            except Exception as e:
                raise e

    def doContainsFromHashPhoto(self, photo_hash):
        """ Respond true if already contains the photo """

        request = "SELECT * FROM photos WHERE hash = '{0}'".format(photo_hash)
        self.execute(request)
        entries = self._cursor_db.fetchone()
        return entries is not None
    # photo = self.getPhotoProxyFromHash(photo_hash)
    # return photo is not None

    def doContainsPhoto(self, photo):
        """ Respond true if already contains the photo """
        return self.doContainsFromHashPhoto(photo['hash'])

    def size(self):
        """ return the number of handled photos """
        self.execute("""SELECT COUNT(*) FROM photos""")
        _sz = self._cursor_db.fetchone()[0]
        return _sz

    def __len__(self):
        """ return the number of handled photos """
        return self.size()

    def createEvent(self, events):
        self.execute(
            "INSERT INTO events ('eventname') VALUES (\"{0}\")".format(
                events))
        eventid = self._cursor_db.lastrowid
        return eventid

    def getEventIDFromName(self, events):
        if events is None:
            event = ""
        if events == [None]:
            event = ""
        if isinstance(events, list):
            event = "/".join(events)

        self.execute(
            "SELECT eventid FROM events WHERE eventname = \"{0}\"".format(
                event))
        eventid = self._cursor_db.fetchone()

        if eventid is None:
            eventid = self.createEvent(event)
        else:
            eventid = eventid[0]
        return eventid

    def getByteSize(self):
        """ Return the size occupied by the photo """
        self.execute("""SELECT sum(filesize) from photos""")
        file_size = self._cursor_db.fetchone()[0]
        return file_size

    def add(self, photo, recompute_hash=False):
        """ Add the photo to the database """

        events = photo.getEvents()
        event_id = self.getEventIDFromName(events)

        photo_hash = photo.getHash(recompute_hash=recompute_hash)
        if self.isPhotoInTrash(hash):
            return

        fname = photo.getFilename()
        rel_fname = os.path.relpath(fname, start=self.getRootPath())
        creation_date = Photo.getTimeStamp(photo.getDate())
        last_update = Photo.getTimeStamp(photo.getLastUpdate())
        file_size = photo.getSize()

        try:
            self.execute("""INSERT INTO photos VALUES (
            '{0}',"{1}",'{2}','{3}','{4}','{5}'
            )
            """, photo_hash, rel_fname, creation_date,
                         last_update, event_id, file_size)
        except Exception as e:
            if str(e) == 'UNIQUE constraint failed: photos.hash':
                photo_proxy = self.getPhotoProxyFromHash(photo_hash)
                fname = photo_proxy.getAbsPath()

                if fname is None:
                    raise Exception(
                        'impossible situation: hash = {0} {1}'.format(
                            photo.getHash(True), photo.getHash(True)))
                # the file do not exist anymore: the file was displaced
                if not os.path.exists(fname):
                    # print fname
                    # print photo_proxy.getRelativePath()
                    self.treatDisplacedPhoto(photo, photo_proxy)
                elif not rel_fname == photo_proxy.getRelativePath():
                    photo_tmp = Photo.createPhoto(fname)
                    self.printInfo(
                        rel_fname + " == " + photo_proxy.getRelativePath())
                    self.printInfo(
                        "deletion of duplicate file " +
                        str(photo.getFilename()))

                    if photo_tmp.getEvents() == photo_proxy.getEvents():
                        # ans = raw_input("Delete duplicate ?")
                        # ans = ans.lower()
                        # if ans == 'y': os.remove(photo.getFilename())
                        os.remove(photo.getFilename())

            else:
                raise e

    def treatDisplacedPhoto(self, photo, photo_proxy):
        print(photo.getFilename(), photo_proxy.getRelativePath())
        fname = os.path.relpath(photo.getFilename(), start=self.getRootPath())
        hash1 = photo.getHash()
        hash2 = photo_proxy.photo_hash
        if hash1 != hash2:
            raise RuntimeError('not matching hash')

        photo_proxy.updateFilename(fname)
        self.commit()

    def scanTrashBin(self):
        """ Scan a trashbin directory """
        dir_to_scan = os.path.join(self._root_path, '.trash_bin')
        if not os.path.isdir(dir_to_scan):
            return
        trash_db = PhotoDB(dir_to_scan, check_db_path=False)
        trash_db.scanPhotos()
        for photo_hash in trash_db.iterateHash():
            self.addToTrashTable(photo_hash)

    def scanPhotos(self, recompute_hash=False, from_input=False):
        """ Scan a complete directory """
        self.scanTrashBin()
        photo_list = PhotoList()

        if from_input is False:
            self.printInfo("Scanning " + self._root_path)
            photo_list.recursive_fill(self._root_path)
        else:
            photo_list.read_from_standard_input()

        photo_list.make_files_relative(self._root_path)

        cpt = 0
        n_files = len(photo_list)

        for f in photo_list:
            head, tail = os.path.split(f)
            while (head != '') and (head != '/'):
                head, tail = os.path.split(head)
            main_dir = tail
            if os.path.basename(main_dir) == '.trash_bin':
                continue

            full_name = os.path.join(self._root_path, f)
            # try:
            photo = Photo.createPhoto(full_name)
            # except Exception as e:
            # print(e)
            # continue

            events_list = photo.getEvents()
            if events_list is None:
                events = ""
            if isinstance(events_list, list):
                events = "/".join([str(e) for e in events_list])
            self.add(photo, recompute_hash=recompute_hash)
            self.printInfo("{0:.6}\t{1}\t{2}".format(photo.getHash(),
                                                     photo.getFilename(),
                                                     events))
            self.setTaskCompleteness(100. * cpt / n_files)
            cpt += 1
        self.commit()

    def checkSanity(self):
        """ TODO """

        self.checkFileDuplicates()
        self.checkFileExistence()

    def checkFileDuplicates(self):
        """ Check duplicate filenames in the database """

        request = """
select hash, filename from photos where filename in
( select filename from photos group by filename having ( count(filename) > 1 )
) order by filename"""
        self.execute(request)
        # _dups = self._cursor_db.fetchall()
        dups = {}
        for h, f in dups:
            print('{0}\t{1}'.format(h, f))
        # raise Exception('AAAAAAA')

    def checkFileExistence(self):
        """ Displace files in order to make a consistent tree """

        for photo in self:
            # rel_fname = photo['filename']
            fname = photo.getAbsPath()
            if not os.path.exists(fname):
                photo.remove()

    def searchDisplacedPhotos(self):
        """ Scan the photo that have been displaced """
        pass

    def __iter__(self):
        """ Make an iterator over the photos """
        return PhotoIterator(self)

    def iterateHash(self, **kwargs):
        """ Make an iterator over the photos """
        return PhotoIterator(self, it_hash=True, **kwargs)

    def initSQLlite(self, filename):
        """ Initialize the SQL database """

        self._db_file = filename
        self._conn_db = sqlite3.connect(self._db_file)
        self._cursor_db = self._conn_db.cursor()

        creation_requests = [
            """
CREATE TABLE events
(
eventid INTEGER PRIMARY KEY,
eventname TEXT UNIQUE
);
            """,
            """
CREATE TABLE photos
(
hash TEXT NOT NULL PRIMARY KEY,
filename TEXT,
creation_date DATE NOT NULL,
last_update DATE NOT NULL,
eventid INTEGER,
filesize INTEGER NOT NULL,
FOREIGN KEY(eventid) REFERENCES events(eventid)
);
            """,
            """
CREATE TABLE albums
(
albumid INTEGER PRIMARY KEY,
name text NOT NULL UNIQUE
);
            """,
            """
CREATE TABLE photoAlbums
(
id INTEGER PRIMARY KEY,
photoid INTEGER NOT NULL,
albumid INTEGER NOT NULL,
FOREIGN KEY(photoid) REFERENCES photos(hash),
FOREIGN KEY(albumid) REFERENCES albums(albumid)
);
            """,
            """
CREATE TABLE trashBin
(
photoid TEXT NOT NULL PRIMARY KEY
);
            """
        ]

        for r in creation_requests:
            try:
                self.execute(r)
                self.commit()

            except Exception as e:
                pass
                # self.printInfo(e)

    def commit(self):
        """ Commit the pending changes of the database """
        self._conn_db.commit()

    def execute(self, request, *args):
        """ Execute a request """

        request = request.format(*args)
        try:
            self._cursor_db.execute(request)
        except Exception as e:
            ex = Exception(e)
            ex.request = request
            raise ex

# --------------------------------------------------------------- #


def main(argv=None):

    if argv is None:
        argv = sys.argv

    import argparse
    parser = argparse.ArgumentParser(
        description='Doudouz application for photo management')

    # option for the checking feature
    parser.add_argument(
        '--check_sanity', action='store_true',
        help='check several criteria for sanity of the database')

    # option for the scanning feature
    parser.add_argument('--scan', action='store_true',
                        help='request the database to be scanned for photos')
    parser.add_argument(
        '--recompute_hash', action='store_true',
        help=('when scanning this specifies to force to recompute'
              ' the internal hash'))

    parser.add_argument('-', '--from_input', action='store_true',
                        help='read from standard input')

    # option for the deletion feature
    parser.add_argument('--delete', action='store_true',
                        help='request the database to delete a single photo')
    parser.add_argument('--from_hash', type=str,
                        help='delete photo from hash')
    parser.add_argument('--from_filename', type=str,
                        help='delete photo from filename')
    parser.add_argument(
        '--already_in_trash', action='store_true',
        help='delete photos that are registered in the trash bin')

    args = vars(parser.parse_args())

    # print args
    photo_db = PhotoDB(db_path=os.getcwd())

    if args['check_sanity'] is True:
        photo_db.checkSanity()

    if args['scan'] is True:
        photo_db.scanPhotos(
            recompute_hash=args['recompute_hash'],
            from_input=args['from_input']
        )

    if args['delete'] is True:
        if args['already_in_trash'] is True:
            photo_db.removePhotoRegisteredInTrash()
        elif 'from_filename' in args:
            try:
                filename = args['from_filename']
                photo = Photo.createPhoto(filename)
                photo_db.remove(photo)
            except NotExistingFile:
                relative_path = photo_db.getRelativePath(filename)
                print("delete {0}".format(relative_path))
                photo_db.execute("""
DELETE FROM photos WHERE filename='{0}'
""".format(relative_path))
                photo_db.commit()

        else:
            raise Exception('to be implemented')

# ------------------------------------------------------------- #


if __name__ == '__main__':
    main()
