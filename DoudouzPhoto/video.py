#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Module for synchronizing two database
"""

import os
import sys
import shutil
import hashlib
import pickle
from . import metadata_exiftool
from . import photo
from .photo import Photo
# -------------------------------------------------------------------------- #


class Video(Photo):

    def __init__(self, load_from_file):
        """ Constructor """
        Photo.__init__(self, load_from_file)
        self.date_key = "CreateDate"

    def loadInfoFromFile(self, path):
        """ Load info (metadata) from file (not the image itself) """
        if not os.path.exists(path):
            raise Exception('not existing photo: ' + path)

        self._metadata = metadata_exiftool.MetaDataManagerExiftool(path)
        self.fetchCustomTags()

    def fetchCustomTags(self):

        tag_file_name = self.getTagFileName()
        if os.path.exists(tag_file_name):
            print("read old format")
            self._custom_tags.update(pickle.load(open(tag_file_name, 'r')))
            if self.extension == 'avi':
                return self._custom_tags
            self.saveCustomTags()
            os.remove(tag_file_name)
            return self._custom_tags
        else:
            return Photo.fetchCustomTags(self)

    def saveCustomTags(self):
        if not self.extension == 'avi':
            Photo.saveCustomTags(self)
            return

        if self._custom_tags is None:
            return

        tag_file_name = self.getTagFileName()
        pickle.dump(self._custom_tags, open(tag_file_name, 'wb'))

    def recomputeHash(self):
        """ Hash the video data """
        import cv2
        cap = cv2.VideoCapture(self._filename)

        cumul_hash = ""
        cpt = 0
        while(cap.isOpened()):
            ret, frame = cap.read()
            if (cpt % 100 == 0):
                sys.stderr.write('.')
            if frame is None:
                break
            cumul_hash += hashlib.sha1(frame).hexdigest()
            cpt += 1

        cap.release()
        _hash = str(cumul_hash).encode('utf8')
        _hash = hashlib.sha1(_hash).hexdigest()

        return _hash

    def show(self):
        """ Plot the photo using matplotlib """
        pass

    def getNumpy(self):
        """ get a Numpy from the photo """
        raise RuntimeError('to_be_implemented')

    def moveTagFile(self, newpath):
        tag_file = self.getTagFileName()
        tag_file_basename = os.path.basename(tag_file)
        _dir = os.path.dirname(newpath)
        new_tag_file = os.path.join(_dir, tag_file_basename)
        try:
            os.rename(tag_file, new_tag_file)
        except:
            # raise Exception("cannot move tag file {0} to {1}".format(
            # tag_file,new_tag_file))
            pass

    def copyTagFile(self, newpath):
        tag_file = self.getTagFileName()
        tag_file_basename = os.path.basename(tag_file)
        _dir = os.path.dirname(newpath)
        new_tag_file = os.path.join(_dir, tag_file_basename)
        try:
            shutil.copy2(tag_file, new_tag_file)
        except:
            pass

    def remove(self):
        tag_file = self.getTagFileName()
        if os.path.exists(tag_file):
            os.remove(tag_file)
        Photo.remove(self)

    def move(self, new_path):
        """ Displace a file to another location """

        if new_path == self.getFilename():
            return
        new_path = self.findNonOverlappingNewName(new_path)

        print("mv {0} {1}".format(self.getFilename(), new_path))
        self.preparePath(new_path)
        os.rename(self.getFilename(), new_path)
        self.moveTagFile(new_path)

    def copy(self, new_path, overwrite=False):
        """ Copy a file """

        new_photo = Photo.copy(self, new_path, overwrite)
        self.copyTagFile(new_path)
        return new_photo

    def getTagFileName(self):
        fname = self.getFilename()
        _dir = os.path.dirname(fname)
        basename, ext = os.path.splitext(os.path.basename(fname))
        tag_file_name = os.path.join(
            _dir, '.' + basename+'_custom_tags.doudouzTags')
        return tag_file_name


# -------------------------------------------------------------------------- #

if __name__ == '__main__':
    photo.main()
