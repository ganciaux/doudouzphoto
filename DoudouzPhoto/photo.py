#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Module containing class Photo
"""

# ----------------------------------------------------------- #

import time
import rawpy
import os
import cv2
import re
import pickle
import jsonpickle
import json
import hashlib
import subprocess
from datetime import datetime, timedelta
from DoudouzPhoto import metadata_pyexiv2 as metadata_pyexiv2

# ----------------------------------------------------------- #


class SameFileExisting(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr(self.value)
# ----------------------------------------------------------- #


class NotExistingFile(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr(self.value)

# ----------------------------------------------------------- #


def guessFileType(filename):
    proc = subprocess.Popen(['file', filename], stdout=subprocess.PIPE)
    for line in proc.stdout:
        _l = line.decode('utf8').rstrip()
        _l = _l.split(':')[1]
        _l = _l.strip()
        return _l

# ----------------------------------------------------------- #


class Photo:

    """
class Photo: Documentation TODO
    """

    @classmethod
    def createPhoto(cls, filename):
        basename, ext = os.path.splitext(filename)
        extension = ext[1:].lower()
        if extension in ['mov', '3gp', 'avi', 'mp4']:
            from DoudouzPhoto.video import Video
            return Video(filename)
        elif extension in ['jpeg', 'jpg', 'nef', 'cr2']:

            ignore_types = ['AppleDouble encoded Macintosh file']
            filetype = guessFileType(filename)
            if filetype in ignore_types:
                raise Exception(
                    'for file: {0}\nignored file type: {1}'.format(
                        filename, filetype))
            return Photo(filename)

        raise Exception('{0}: unknown extension: {1}'.format(filename,
                                                             extension))

    @classmethod
    def getTimeStamp(cls, d):
        if d is None:
            return None
        ts = time.mktime(d.timetuple())
        return ts

    @classmethod
    def getDateFromTimeStamp(cls, ts):
        if ts is None or ts == 'None':
            raise Exception('invalid timestamp')

        return datetime.fromtimestamp(ts)

    def __init__(self, load_from_file):

        self. _setFilename(load_from_file)
        self._custom_tags = dict()
        self.loadInfoFromFile(load_from_file)
        self.date_key = 'Exif.Image.DateTime'

    def getFilename(self):
        """ Return the filename of the photo """
        return self._filename

    def getOldFilename(self):
        """ Return the filename from metadata """
        return self['filename']

    def _setFilename(self, path):
        if isinstance(path, bytes):
            path = path.encode('utf8')
        self._filename = path
        basename, ext = os.path.splitext(path)
        self.extension = ext[1:].lower()

    def loadInfoFromFile(self, path):
        """ Load info (metadata) from file (not the image itself) """
        if not os.path.exists(path):
            raise NotExistingFile('not existing photo: ' + path)

        self._metadata = metadata_pyexiv2.MetaDataManagerPyExiv2(path)
        # self._metadata = metadata_exiftool.MetaDataManagerExiftool(path)
        self.fetchCustomTags()

    def writeInfoToFile(self):
        """ Write the metadata to file """
        self._metadata.write()

    def setTags(self, tags):
        """ Set tags provided in the dictionary """
        for k, value in tags.items():
            self.setTag(k, value)

    def setTag(self, key, value):
        """ Set a single tag """
        self._metadata.setTag(key, value)

    def getTag(self, key):
        """ get a single tag """
        return self._metadata.getTag(key)

    def getNumpy(self):
        """ get a Numpy from the photo """
        if self.extension == 'cr2':
            image = rawpy.imread(self.getFilename()).postprocess()
        else:
            image = cv2.imread(self.getFilename())
        return image

    def show(self):
        """ Plot the photo using matplotlib """
        import cv2
        cv2.namedWindow(self.getFilename(), cv2.WINDOW_NORMAL)
        cv2.imshow(self.getFilename(), self.getNumpy())
        cv2.waitKey(1000)
        cv2.destroyAllWindows()

    def __str__(self):
        """ Convert to string: to be used with the print function """

        member_list = [str(e[0]) + ": " + str(e[1])
                       for e in self.__dict__.items()]
        sstr = "\n".join(member_list)
        sstr += "\nCreation Date: " + str(self.getDate())
        meta_data = [k + " " + str(self.getTag(k)) for k in self._metadata]
        sstr += "\n" + "\n".join(meta_data)
        return sstr

    def isSame(self, other_photo):
        """ Check is the photo is the same: based on Hash """
        if not self.getHash() == other_photo.getHash():
            return False
        return True

    def findNonOverlappingNewName(self, new_path):
        cpt = 0

        while os.path.exists(new_path):
            # print(new_path + " " + self.getFilename())
            to_compare = Photo.createPhoto(new_path)
            if self.isSame(to_compare):
                raise SameFileExisting(
                    'Found exact same file: database is not aware but I should'
                    ' not create a new file for this:\n{0} = {1}'.format(
                        self.getFilename(), new_path))

            basename, ext = os.path.splitext(os.path.basename(new_path))
            dirname = os.path.dirname(new_path)
            new_path = os.path.join(
                dirname, basename + ".{0}{1}".format(cpt, ext))

            cpt += 1

        return new_path

    def preparePath(self, new_path):
        dirname = os.path.dirname(new_path)
        try:
            os.makedirs(dirname)
        except Exception:
            pass

    def remove(self):
        os.remove(self.getFilename())

    def move(self, new_path):
        """ Displace a file to another location """

        if new_path == self.getFilename():
            return
        new_path = self.findNonOverlappingNewName(new_path)

        # print "mv {0} {1}".format(self.getFilename(),new_path)
        self.preparePath(new_path)
        os.rename(self.getFilename(), new_path)

    def copy(self, new_path, overwrite=False):
        """ Copy a file """

        if new_path == self.getFilename():
            return

        if overwrite is True:
            print("overwrite {0} -> {1}".format(self.getFilename(), new_path))
            os.system('rm "{0}"'.format(new_path))
            os.system('cp "{0}" "{1}"'.format(self.getFilename(), new_path))
            # shutil.copy2(self.getFilename(),new_path)
        else:
            try:
                new_path = self.findNonOverlappingNewName(new_path)
                # print("cp {0} {1}".format(self.getFilename(), new_path))
                self.preparePath(new_path)
                os.system('cp "{0}" "{1}"'.format(self.getFilename(),
                                                  new_path))
                # shutil.copy(self.getFilename(),new_path)
            except SameFileExisting:
                pass

        # print("construct new photo")
        new_photo = Photo.createPhoto(new_path)
        return new_photo

    def setEvents(self, e_list):
        """ Set the event of the file """

        if not isinstance(e_list, list):
            e_list = [e_list]
        custom_tags = self.fetchCustomTags()
        custom_tags['events'] = e_list
        self.saveCustomTags()

    def recomputeHash(self):
        """ Hash the image data """
        im = self.getNumpy()
        if im is None:
            raise RuntimeError("failed to load image content for: " +
                               self._filename)
        _hash = hashlib.sha1(im).hexdigest()
        return _hash

    def _setHash(self, _hash):
        custom_tags = self.fetchCustomTags()
        custom_tags['hash'] = _hash
        self.saveCustomTags()

    def getHash(self, recompute_hash=False):
        """ get the Hash of the photo """

        custom_tags = self.fetchCustomTags()
        if (('hash' not in custom_tags) or
            isinstance(custom_tags['hash'],
                       int) or
                recompute_hash):
            print("Computing hash for: {0}".format(self._filename))
            _hash = self.recomputeHash()
            self._setHash(_hash)

        return custom_tags['hash']

    def saveCustomTags(self):

        if self._custom_tags is None:
            return

        flat_obj = jsonpickle.dumps(self._custom_tags)
        encoded_tag = "DoudouzPhotoDBStart" + flat_obj + "DoudouzPhotoDBEnd"
        self.setTag('UserComment', encoded_tag)

    def fetchCustomTags(self):
        key = 'UserComment'
        try:
            custom_tags = self.getTag(key)
        except KeyError:
            return self._custom_tags

        # print(key, custom_tags)
        m = re.match("DoudouzPhotoDBStart(.*?)DoudouzPhotoDBEnd",
                     custom_tags, flags=re.DOTALL)
        if m:
            encoded_tag = m.group(1)
            try:
                loaded_tags = jsonpickle.loads(encoded_tag)
            except json.decoder.JSONDecodeError:
                enc_tag = encoded_tag.encode('utf8')
                # print('BBBBBBBBBB', enc_tag, self._filename)
                loaded_tags = pickle.loads(
                    enc_tag, encoding='latin1')
            self._custom_tags.update(loaded_tags)

        return self._custom_tags

    def getEvents(self):
        """ Retreive the list of associated events """

        custom_tags = self.fetchCustomTags()
        if 'events' in custom_tags:
            events = custom_tags['events']
            # if len(events) == 0: return None
            # while events[0] == '..' : events = events[1:]
            return events

        return [""]

    def getSize(self):
        """ return the size of the photo """
        return os.path.getsize(self.getFilename())

    def getLastUpdate(self):
        """ return the time of last modification """
        timestamp = os.path.getmtime(self.getFilename())
        date = datetime.fromtimestamp(timestamp)
        return date

    def getDate(self):
        """ Return the origin date of the photo """

        keys = ['DateTimeOriginal', 'DateTime', 'DateTimeOriginal',
                'DateUTC', 'DateAcquired', 'CreateDate',
                'Exif.Image.DateTime']

        date = None

        for k in keys:
            try:
                date = self.getTag(k)
                break
            except KeyError:
                pass

        try:
            if not isinstance(date, datetime):
                date_tmp = int(date)
                date = date_tmp
        except TypeError:
            # catch the fact that date is not the representation
            # of an integer
            pass
        except ValueError:
            # catch the fact that date is not the representation
            # of an integer
            pass

        if isinstance(date, datetime):
            return date
        elif isinstance(date, int):
            # Date Time Original
            # measured in seconds relatively to Jan 01, 1904, 0:00:00 GMT+0h
            seconds = int(date)
            ref_time = datetime(1904, 1, 1)
            # ref_time = datetime(1945,1,1)
            delta = timedelta(seconds=seconds)
            date = ref_time + delta
            return date
        elif isinstance(date, bytes) or isinstance(date, str):
            formats = ["%Y-%m-%d %H:%M:%S", "%Y:%m:%d %H:%M:%S",
                       "%Y/%m/%d/ %H:%M ", "%Y-%m-%dT%H:%M:%S",
                       "%a %b %d %H:%M:%S %Y "]
            date_temp = None
            for f in formats:
                try:
                    _date = date.split('.')[0]
                    date_temp = datetime.strptime(_date, f)
                    break
                except Exception:
                    # print e
                    pass
            date = date_temp
            # print date
            return date
        return None

    def setDate(self, date):
        """ Set the origin date of the photo """
        key = self.date_key
        self.setTag(key, date)

    def __getitem__(self, index):
        return self._custom_tags[index.lower()]

    def __setitem__(self, index, value):
        self._custom_tags[index.lower()] = value


# -------------------------------------------------------------------------- #

def main():

    import sys

    fname = sys.argv[1]
    recompute_hash = False
    if len(sys.argv) == 3 and sys.argv[2] == 'recompute_hash':
        recompute_hash = True

    photo = Photo.createPhoto(fname)
    print(photo)
    print(str(photo.getDate()) + ":" + fname)
    print(photo.getHash(recompute_hash) + ":" + fname)


if __name__ == '__main__':
    main()
