#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------- #
"""
Module containing class PhotoProxy
"""
# ----------------------------------------------------------- #
import os
from .photo import Photo
# ----------------------------------------------------------- #


class PhotoNotInDataBase(Exception):
    def __init__(self, photo_hash):
        self.photo_hash = photo_hash

    def __str__(self):
        return f"{self.photo_hash} not present in database"


class PhotoProxy(object):
    """
This is the photo representation issued from the database:
  it wraps a table entry
    """

    def __init__(self, photo_hash, photo_db):

        self.photo_db = photo_db
        self.photo_hash = photo_hash
        self.photo_db.execute("PRAGMA table_info(photos)")
        cols = self.photo_db.fetch_results()
        col_names = [c[1] for c in cols]
        request = "SELECT * FROM photos WHERE hash = '{0}'".format(photo_hash)
        self.photo_db.execute(request)
        self.entries = self.photo_db.fetch_one_result()
        if self.entries is None:
            raise PhotoNotInDataBase(photo_hash)
        self.entries = dict(zip(col_names, self.entries))

        for k in self.entries.keys():
            if isinstance(self.entries[k], bytes):
                self.entries[k] = self.entries[k].encode('utf8')

        if not self.entries['creation_date'] == 'None':
            self.entries['creation_date'] = Photo.getDateFromTimeStamp(
                self.entries['creation_date'])

        if not self.entries['last_update'] == 'None':
            self.entries['last_update'] = Photo.getDateFromTimeStamp(
                self.entries['last_update'])

    def __getitem__(self, index):
        return self.entries[index.lower()]

    def __setitem__(self, index, value):
        self.entries[index.lower()] = value

    def updateFilename(self, filename):
        if self.entries['filename'] == filename:
            return

        self.photo_db.execute(
            "UPDATE photos SET filename='{0}' WHERE hash='{1}'".format(
                filename, self.photo_hash))

        self.photo_db.commit()

    def setEvents(self, events):
        """ set the event of a photo and update database """
        eventid = self.photo_db.getEventIDFromName(events)
        self.photo_db.execute(
            "UPDATE photos SET eventid='{0}' WHERE hash='{1}'".format(
                eventid, self.photo_hash))

        try:
            photo = Photo.createPhoto(self.entries['filename'])
            photo.setEvents(events)
        except Exception as e:
            print(e)
            raise RuntimeError(
                "did you move the file manually ? "
                "=> try update the photos")

        self.photo_db.commit()

    def setDate(self, date):
        """ set the date of a photo and update database """
        command = """
UPDATE photos SET creation_date='{0}' WHERE hash='{1}'
""".format(Photo.getTimeStamp(date), self.entries['hash'])
        # print(command)
        self.photo_db.execute(command)
        photo = Photo.createPhoto(self.entries['filename'])
        photo.setDate(date)
        self.photo_db.commit()

    def moveToTrashFromHash(self, photo_hash):
        raise Exception('needs revision')
        # print photo_hash
        # photo = self.getPhotoProxyFromHash(photo_hash)
        # rel_path = self.getRelativePath(photo)
        # new_path = os.path.join(self._root_path,'.trash_bin',rel_path)
        # print new_path
        # photo.move(new_path)
        # self.addToTrashTable(photo_hash)
        # self.execute(
        # """DELETE FROM photos WHERE hash='{0}'""".format(photo_hash))
        # self.commit()

    def remove(self, deep=False):
        """ Documentation TODO """
        self.photo_db.execute("""DELETE FROM photos WHERE hash='{0}'""".format(
            self.entries['hash']))

        if deep:
            photo = Photo.createPhoto(self.getAbsPath())
            photo.remove()
        self.photo_db.commit()

    def move(self, new_path):
        """ Move a photo on disk and database """

        if new_path == self.getRelativePath():
            return

        photo = Photo.createPhoto(self.getAbsPath())
        self.photo_db.printInfo("mv {0} {1}".format(
            self.getRelativePath(), new_path))
        self.photo_db.execute(
            """UPDATE photos SET filename="{0}" WHERE hash='{1}'""".format(
                new_path, photo.getHash()))
        full_new_path = os.path.join(self.photo_db.getRootPath(), new_path)
        photo.move(full_new_path)
        self.photo_db.commit()

    def getEvents(self):
        eid = self.entries['eventid']
        self.photo_db.execute(
            "SELECT eventname FROM events WHERE eventid = \"{0}\"".format(eid))
        events = self.photo_db.fetch_one_result()[0]
        events = events.split(os.sep)
        return events

    def getAbsPath(self):
        """
return the absolute path from a path relative
to the current database
"""

        return os.path.join(self.photo_db.getRootPath(),
                            self.entries['filename'])

    def copy(self, new_name):
        _photo = Photo.createPhoto(self.getAbsPath())
        return _photo.copy(new_name)

    def getBasename(self):
        return os.path.basename(self.entries['filename'])

    def getRelativePath(self):
        """ return the path relative to the current database """
        fname = self.entries['filename']
        return fname

    def getYearMonthEventFilePath(self):
        """ Return the correct path for the current photo """

        date_base = self.getYearMonthPath()
        if date_base is None:
            return self.entries['filename']

        basename = self.getBasename()
        events = self.getEvents()
        return os.path.join(date_base, *(events+[basename]))

    def getDate(self):
        date = self.entries['creation_date']
        return date

    def getYearMonthPath(self):
        """ Return the correct path for the current photo """

        date = self.entries['creation_date']

        if (date is None) or (date == 'None'):
            return None

        year = date.strftime('%Y')
        month = date.strftime('%B').lower()

        return os.path.join(year, month)

    def updatePhoto(self, photo):
        raise Exception('needs revision')

        # photo_hash = photo.getHash()
        # raise Exception('need code review')
        # new_eventid = self.getEventIDFromName(photo.getEvents())
        # new_filesize = photo.getSize()
        #
        #
        # self.execute("""SELECT eventid,filesize FROM photos
        # WHERE hash = '{0}'
        # """.format(photo_hash))
        #
        # eventid, filesize = self._cursor_db.fetchone()
        # if eventid == new_eventid and new_filesize == filesize:
        #     return
        #
        # if photo.getEvents() is None:
        #     return
        #
        # self.execute(
        # """SELECT eventname FROM events
        # WHERE eventid = '{0}'""".format(eventid))
        #
        # event = self._cursor_db.fetchone()[0]
        #
        # print "need to update {0}:{1} != {2}:{3}, {4} != {5}".format(
        #     event, eventid, photo.getEvents(),
        #     new_eventid, new_filesize, filesize)
        #
        # photo_to_update = self.getPhotoFromHash(photo_hash)
        #
        # # print "geteventid"
        # # photo_to_update.setEvents(photo.getEvents())
        # event_id = self.getEventID(photo)
        #
        # # print "copy photo"
        # photo.copy(photo_to_update.getFilename(), overwrite=True)
        # # print "getLastUpdate"
        # last_update = photo.getLastUpdate()
        # # print "filesize"
        # file_size = photo.getSize()
        #
        # # print "update database"
        # self.execute("""UPDATE photos SET
        # last_update = '{0}', eventid = '{1}', filesize = '{2}'
        # WHERE hash = '{3}'""".format(
        #     last_update, event_id, file_size, photo_hash))
        #
        # # print "commit"
        # self.commit()
        # # print "done"

        #    def getEventID(self,photo):
        #        events = photo.getEvents()
        #        print events
        #        if events is None: events = ""
        #        if events == [None]: events = ""
        #        else: events = "/".join(events)
        #        return self.getEventIDFromName(events)
