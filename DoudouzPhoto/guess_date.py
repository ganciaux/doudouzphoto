#!/usr/bin/env python3
# -*- coding: utf-8 -*-
################################################################
import calendar
import os
from datetime import datetime
from photo import Photo


def guessDate(filename):
    dirname = os.path.dirname(filename)
    mean_timestamp = 0
    cpt = 0
    for fname in os.listdir(dirname):

        try:
            full_name = os.path.join(dirname, fname)
            photo = Photo.createPhoto(full_name)
            date = photo.getDate()
            if date is not None:
                timestamp = calendar.timegm(date.utctimetuple())
                mean_timestamp += timestamp
                cpt += 1

        except Exception as e:
            #            print e
            pass

    # if cpt == 0: raise Exception('cannot guess for file: ' + filename)
    if cpt == 0:
        return None

    mean_timestamp /= cpt
    mean_date = datetime.fromtimestamp(mean_timestamp)
    # ans = raw_input("The mean date for the directory {0} is: {1}\n
    # Do you want to set photos with unknown dates to that one ?"
    # .format(dirname,mean_date))

    # if not ans == 'y': return None

    for fname in os.listdir(dirname):
        try:
            full_name = os.path.join(dirname, fname)
            photo = Photo.createPhoto(full_name)
            photo.setDate(mean_date)
        except Exception as e:
            pass

    return mean_date
