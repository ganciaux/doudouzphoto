#!/usr/bin/env python3
"""
@file   coding_convention.py

@author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
"""

try:
    import unittest
    import os
    import pep8
    # from pylint import epylint as lint
    import DoudouzPhoto

except ImportError as err:
    import sys
    print(err)
    sys.exit(str(err))


class Pep8Test(unittest.TestCase):
    """
    Test for pep8 conformity
    """

    def setUp(self):
        """
        builds a list of source files. If you find a smarter way, please let me
        know
        """
        print()  # for emacs to evaluate the first line of errors
        self.mod_files = list()
        for dirpath, _, filenames in os.walk(DoudouzPhoto.__path__[0]):
            self.mod_files += [os.path.join(dirpath, filename)
                               for filename in filenames
                               if filename.endswith((".py", ".pyx"))]

    def test_pep8_conformity(self):
        """
        check all files for pep8 conformity
        """
        pep8style = pep8.StyleGuide()
        pep8style.check_files((mod_file for mod_file in self.mod_files))

    # def test_pylint_bitchiness(self):
    #    print()  # for emacs to evaluate the first line of errors
    #    options = ' --rcfile=tests/pylint.rc --disable=locally-disabled '
    #    command_options = " ".join(self.mod_files) + options
    #    with warnings.catch_warnings():
    #        warnings.simplefilter("ignore")
    #
    #        lint.py_run(command_options=command_options,
    #                    script="epylint3", )
