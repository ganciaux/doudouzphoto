#!/usr/bin/env python3

import pytest
import os
import cv2
import rawpy
import hashlib
from DoudouzPhoto.photo import Photo
from datetime import datetime


def compute_hash(fname):
    if os.path.splitext(fname)[1].lower() == '.cr2':
        np_image = rawpy.imread(fname).postprocess()
        print(np_image.shape)
    else:
        np_image = cv2.imread(fname, cv2.IMREAD_UNCHANGED)
    print(fname)
    print(np_image.shape)
    print(np_image.dtype)
    _hash = hashlib.sha1(np_image).hexdigest()
    return _hash, np_image


@pytest.fixture
def load_photos():
    files = os.listdir('images')
    files = [os.path.join('./images', f)
             for f in files if f != '.gitattributes' and
             os.path.splitext(f)[1].lower() != '.mov']
    print(files)
    return files


def test_print_photo(load_photos):
    for fname in load_photos:
        print(fname)
        _p = Photo.createPhoto(fname)
        print(_p)


def test_hash_photo(load_photos):
    for fname in load_photos:
        # if ((os.path.splitext(fname)[1].lower() == '.cr2') or
        #        (os.path.splitext(fname)[1].lower() == '.mov')):
        #    continue
        _p = Photo.createPhoto(fname)
        _hash, np_image = compute_hash(fname)
        assert (np_image == _p.getNumpy()).all()
        assert _hash == _p.recomputeHash()
        _p2 = Photo.createPhoto(fname)
        assert _p.getHash() == _p2.getHash()


def test_date_photo(load_photos):
    for fname in load_photos:
        _p = Photo.createPhoto(fname)

        _date = _p.getDate()
        assert _date is not None
        print(str(_date) + ":" + fname)

        if os.path.splitext(fname)[1].lower() == '.cr2':
            continue

        now = datetime.today().replace(microsecond=0)
        _p.setDate(now)
        _date = _p.getDate()
        print('AAAA', fname, _date, now)
        assert _date == now
        _p2 = Photo.createPhoto(fname)
        _date = _p2.getDate()
        assert _date == now
